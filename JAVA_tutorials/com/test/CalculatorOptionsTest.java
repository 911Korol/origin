import com.calculator.CalculatorOptions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CalculatorOptionsTest {

    @Mock
    CalculatorOptions calculator;

    @Test
    public void shouldReturnAddedTwoOperands() {
        Mockito.doReturn(5).when(calculator).getFirstOperator();
        Mockito.doReturn(6).when(calculator).getSecondOperator();
        Mockito.when(calculator.summarize()).thenCallRealMethod();
        Assert.assertEquals(11, calculator.summarize());
    }

    @Test
    public void shouldReturnMultipliedTwoOperands(){
        Mockito.doReturn(3).when(calculator).getFirstOperator();
        Mockito.doReturn(5).when(calculator).getSecondOperator();
        Mockito.when(calculator.multiply()).thenCallRealMethod();
        Assert.assertEquals(15,calculator.multiply());
    }

    @Test
    public void shouldReturnDividedTwoOperands(){
        Mockito.doReturn(21).when(calculator).getFirstOperator();
        Mockito.doReturn(3).when(calculator).getSecondOperator();
        Mockito.when(calculator.divide()).thenCallRealMethod();
        Assert.assertEquals(7,calculator.divide());
    }

    @Test
    public void shouldReturnSubtractedTwoOperands(){
        Mockito.doReturn(32).when(calculator).getFirstOperator();
        Mockito.doReturn(3).when(calculator).getSecondOperator();
        Mockito.when(calculator.subtract()).thenCallRealMethod();
        Assert.assertEquals(29,calculator.subtract());
    }

}
