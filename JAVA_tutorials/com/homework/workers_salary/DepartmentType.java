package com.homework.workers_salary;

/**
 * Created by 007 on 07.05.2017.
 */
public enum DepartmentType {
    NEUROLOGYCAL,
    GYNECOLOGYCAL,
    STOMATOLOGYCAL,
    UROLOGYCAL,
    SURGYCAL;
}
