package com.homework.workers_salary;

/**
 * Created by 007 on 06.05.2017.
 */
public class Employee {
    private String name;
    private String surname;
    private EmployeeType employeeType;
    private int dayExperience;
    private DepartmentType departmentType;
    private int departmentId;

    public Employee(String name,
                    String surname,
                    EmployeeType employeeType,
                    DepartmentType department,
                    int experience) {
        this.name = name;
        this.surname = surname;
        this.employeeType = employeeType;
        this.departmentType = department;
        this.dayExperience = experience;
    }

    @Override
    public String toString() {
        return
                "Name of employee: " + name + '\n' +
                "Surname: " + surname + '\n'+
                "Level of employee: " + employeeType + '\n'+
                "Working day per mounth: " + dayExperience + '\n'+
                "Department: " + departmentType + '\n';
    }

    public String getName() {
        return name;
    }

    public DepartmentType getDepartmentType() {
        return departmentType;
    }

    public String getSurname() {
        return surname;
    }

    public EmployeeType getEmployeeType() {
        return employeeType;
    }

    public int getDayExperience() {
        return dayExperience;
    }
}
