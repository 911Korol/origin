package com.homework.workers_salary;

/**
 * Created by 007 on 07.05.2017.
 */
public class Department {
    private DepartmentType department;
    private String headOfDepartment;
    private int id;


    public Department(DepartmentType department, String headOfDepartment) {
        this.department = department;
        this.headOfDepartment = headOfDepartment;
    }

    public DepartmentType getDepartment() {
        return department;
    }

    public String getHeadOfDepartment() {
        return headOfDepartment;
    }
}
