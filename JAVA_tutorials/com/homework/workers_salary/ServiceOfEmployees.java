package com.homework.workers_salary;

/**
 * Created by 007 on 06.05.2017.
 */
public class ServiceOfEmployees {
    private Employee[] listOfEmployees;
    ServiceOfDepartment serviceOfDepartment = new ServiceOfDepartment();
    private Employee formOfEmployee;

    public ServiceOfEmployees() {
        listOfEmployees = new Employee[]{
                new Employee("Luk", "One", EmployeeType.MANAGER, DepartmentType.NEUROLOGYCAL, 2),
                new Employee("Lik", "Ore", EmployeeType.MANAGER, DepartmentType.STOMATOLOGYCAL, 12),
                new Employee("Lik", "Onk", EmployeeType.MANAGER, DepartmentType.UROLOGYCAL, 3),
                new Employee("Mike", "Mol", EmployeeType.COMMON, DepartmentType.UROLOGYCAL, 15),
                new Employee("Rex", "Kad", EmployeeType.MANAGER, DepartmentType.GYNECOLOGYCAL, 25),
                new Employee("Tom", "Steel", EmployeeType.MANAGER, DepartmentType.GYNECOLOGYCAL, 25),
                new Employee("Ozzy", "Ann", EmployeeType.COMMON, DepartmentType.SURGYCAL, 4),
                new Employee("Steave", "Li", EmployeeType.COMMON, DepartmentType.SURGYCAL, 3),
                new Employee("Brandon", "Soul", EmployeeType.COMMON, DepartmentType.GYNECOLOGYCAL, 15),
                new Employee("Lina", "Love", EmployeeType.MANAGER, DepartmentType.NEUROLOGYCAL, 20),
                new Employee("Lusy", "Body", EmployeeType.COMMON, DepartmentType.SURGYCAL, 19),
                new Employee("Tara", "Sanny", EmployeeType.COMMON, DepartmentType.UROLOGYCAL, 20),
                new Employee("Lera", "Storm", EmployeeType.COMMON, DepartmentType.SURGYCAL, 22),
        };
    }

    void showEmployeeAndCountSalary(String name, String surname) {
        for (Employee employee : listOfEmployees) {
            if (name.equals(employee.getName()) && surname.equals(employee.getSurname())) {
                formOfEmployee = employee;
            }
        }
        System.out.println(formOfEmployee + "\nSalary : " + serviceOfDepartment.countSalaryAccordingToPosition(formOfEmployee));

    }

    void showPersonWhoseSalaryLess500() {
        System.out.println("Theese persons have a salary less than 500: ");
        System.out.println(" ");
        for (Employee employee : listOfEmployees) {
            if (serviceOfDepartment.countSalaryAccordingToPosition(employee) < 500) {
                System.out.println(employee);
                System.out.println("Salary : " + serviceOfDepartment.countSalaryAccordingToPosition(employee));
                System.out.println("__________________________________");
            }
        }
    }

    void getManagerFromDifferentDepartments() {
        for (Employee employee : listOfEmployees) {
            if (employee.getEmployeeType().equals(EmployeeType.MANAGER)) {
                System.out.println(employee);
            }

        }
    }

    void getEmployeeFromSameDepartmentWithSalaryMore700(DepartmentType departmentType) {
        for (Employee employee : listOfEmployees) {
            if (employee.getDepartmentType().equals(departmentType) && serviceOfDepartment.countSalaryAccordingToPosition(employee) > 700) {
                System.out.println(employee);
                System.out.println(serviceOfDepartment.countSalaryAccordingToPosition(employee));
                System.out.println("____________________________________");
            }
        }
    }
}

