package com.homework.workers_salary;

/**
 * Created by 007 on 07.05.2017.
 */
public class ServiceOfDepartment {
    private Department[] departmentList;

    public ServiceOfDepartment() {
        departmentList = new Department[]{
                new Department(DepartmentType.NEUROLOGYCAL,   "Peleshok"),
                new Department(DepartmentType.STOMATOLOGYCAL, "Beregovaya"),
                new Department(DepartmentType.SURGYCAL,       "Golovnyuk"),
                new Department(DepartmentType.GYNECOLOGYCAL,  "Voronina"),
                new Department(DepartmentType.UROLOGYCAL,     "Novitsyuk"),
        };
    }

    public Department[] getDepartmentList() {
        return departmentList;
    }

    int countSalaryAccordingToPosition(Employee person) {
        /*int salary;
        if (person.getEmployeeType().equals(EmployeeType.MANAGER)) {
            salary = person.getDayExperience() * 1000;
        } else {
            salary = person.getDayExperience() * 100;
        }*/

        int dayExperience = person.getDayExperience();

        return person.getEmployeeType().equals(EmployeeType.MANAGER) ? dayExperience * 1000 : dayExperience * 100;
    }
}
