package com.homework.homeWork02_05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/*Подсчитать количество слов в вводимом предложении, слова разделять только пробелами
 */
public class TestAmountWordInTheString {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inputString = reader.readLine().toLowerCase();
        countWordInString(inputString);
    }

    public static void countWordInString(String inputString) {
        String[] inputStringToArray = inputString.split(" ");
        Map<String, String> amountOfWordInString = new HashMap<>();
        for (String s : inputStringToArray) {
            int countingOfWord = 0;
            for (String s1 : inputStringToArray) {
                if (s.equals(s1)) {
                    countingOfWord++;
                }
                amountOfWordInString.put(("Amount of word '" + s + "' "), (" " + countingOfWord));
            }
        }
        System.out.println(String.format("String has been input by you is: '%search_for_pair_numbers'",inputString));
        for (Map.Entry<String, String> s : amountOfWordInString.entrySet()) {
            System.out.println(s);
        }
    }
}