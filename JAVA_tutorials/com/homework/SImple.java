package com.homework;

import com.company.Lesson41.Part1.Plane;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.sql.SQLDataException;
import java.util.Scanner;

/**
 * Created by 007 on 07.05.2017.
 */
public class SImple {

    private String str = "";

    public void print() {
        System.out.println(str.charAt(0));
    }

    public static void main(String[] args) {

        String str = null;

        System.out.println(str);

        int a = 4;

        int y;
        if (a < 5) {
            y = 10;
        } else {
            y = 20;
        }

//        condition ? statement if condition is true : statement if condition is false;

        y = a < 5 ? 10 : 20;

        new SImple().print();

    }
}


interface Ballable {
    static void print() {

    }
}

class Plant {
    String getName() {
        return "plant";
    }

    Plant getType() {
        System.out.println("Plant");
        return this;
    }

    public static void show() {

    }

    public final void foo() {
        System.out.println("Foo");
    }

    Object calculate(String name, Integer count, BufferedReader bufferedReader) throws IOException {
        return new Object();
    }
}

class Flower extends Plant {


    public final void foo(int x) {
        System.out.println("Foo");
    }

    String getType(int i) {
        return "this";
    }

   /* public static void show() {

    }*/
}

class Tulip extends Flower {
    @Override
    Tulip getType() {
        System.out.println("Tulip");
        return new Tulip();
    }
}

class Test {
    public static void main(String... arg) {
        Plant tulip = new Tulip();
//        tulip.clone();
//        System.out.println(tulip instanceof Plant);
        tulip.getType();

        Flower flower = new Flower();
        flower.show();

    }
}

class A{
    public A method() throws Exception{ // 1
        return new Single();
    }
}
class Single extends A{
    public Single method(String str) throws RuntimeException{ // 2
        return new Single();
    }
    public Single method() throws Exception {  //3
        return new Double();
    }
}
class Double extends Single{
    public void method(Integer digit) throws ClassCastException{      // 4
    }
    public Double method() throws IOException {  // 5
        return new Double();
    }
}

class Test1 {
    private static int s;

    Test1() {
        s = 0;
    }

    public static strictfp void main(String[] args) throws RuntimeException {
        Test1 t = new Test1();
        t.s = 5;

        System.out.println((s == t.s) + " " + s);
    }
}

class Composition {

    private Object object;

//    public Composition(Object object) {
    public Composition() {
//        this.object = object;
        this.object = new Object();
    }

    public static void main(String[] args) {
        Object object = new Object();

//        Composition composition = new Composition(object);
        Composition composition = new Composition();

        int a = 15;

        switch (a) {
            case 12:
                System.out.println("a == 12");
                break;
            case 13:
                System.out.println("a == 13");
                break;
            case 14:
                System.out.println("a == 14");
                break;
            default:
                    System.out.println("Something");
        }
    }
}
