package com.homework.file_manager;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import static java.nio.file.Paths.get;

/**
 * Created by 007 on 26.05.2017.
 */
public class FileManagerOption {
    public void createNewFile(String pathToFile) {
        try {
            Path newFile = get(pathToFile);
            Files.deleteIfExists(newFile);
            Files.createFile(newFile);
        } catch (IOException x) {
            x.printStackTrace();
        }
    }

    public void createNewDirectory(String pathToFile) {
        try {
            Path newDir = get(pathToFile);
            Files.createDirectories(newDir);
            Files.getFileStore(newDir);
        } catch (IOException x) {
            x.printStackTrace();
        }
    }

    public void deleteFileOrDirectory(String pathToFile) {
        try {
            Path removableFile = get(pathToFile);
            Files.delete(removableFile);
        } catch (IOException x) {
            x.printStackTrace();
        }
    }

    public void showDirectoryContent(String pathToFile) {
        try {
            Path showingDirectory = get(pathToFile);
            Files.list(showingDirectory).forEach(System.out::print);
        } catch (IOException x) {
            x.printStackTrace();
        }
    }

   /* public void showDirectoryContent() throws IOException {
        System.out.println("Input path and name of Directory that you want to rename like(./Poem)");
        String pathOfFile = new Scanner(System.in).next();
        //String pattern = "glob:*.java";
        //   PathMatcher matcher = FileSystems.getDefault().getPathMatcher(pattern);
        Path path = Paths.get(pathOfFile);//refactoring
        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes fileAttributes) {
                System.out.println("Matching file : " + file.getFileName());
                return FileVisitResult.CONTINUE;
            }

        });
    }

    public void showFileFromDirectoryViaStream() {
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get("./"))) {
            for (Path path : directoryStream) {
                System.out.println(path.getFileName());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    public void copyFileToAnotherDirectory(String pathToFile, String pathToFile2) {
        try {
            Path copiedFile = get(pathToFile);
            Path destination = get(pathToFile2);
            Files.copy(copiedFile, destination, StandardCopyOption.REPLACE_EXISTING);
            Files.delete(copiedFile);
        } catch (IOException x) {
            x.printStackTrace();
        }
    }

    public void renameFileOrDirectory(String pathToFile, String newName) {
        try {
            Path renamedFileOrDirectory = get(pathToFile);
            Path newFileName = get(newName);
            Files.move(renamedFileOrDirectory, newFileName, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException x) {
            x.printStackTrace();
        }
    }

    public void convertTextFileToPdf(String pathToFile, String pdfFile) {
        Font blueFont = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL, new CMYKColor(255, 0, 0, 0));
        Document document = new Document();
        try {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
            document.open();
            document.add(new Paragraph((new String(Files.readAllBytes(get(pathToFile)))),blueFont));
            document.addCreationDate();
            document.addAuthor("Mihon");
            document.addTitle("Hello");
            document.close();
        } catch (IOException | DocumentException y) {
            y.printStackTrace();
        }
    }
}
