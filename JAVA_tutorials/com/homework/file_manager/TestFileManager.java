package com.homework.file_manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by 007 on 16.05.2017.
 */
public class TestFileManager {
    private static final BufferedReader READER = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) {

        FileManagerOption fileManagerOption = new FileManagerOption();
        while (true) {
            fileManagerMenu();
            try {
                int menuSelection = Integer.parseInt(READER.readLine());
                switch (menuSelection) {
                    case 1: {
                        String nameOfFile = getValueFromUser("Enter file name");
                        fileManagerOption.createNewFile(nameOfFile);
                        break;
                    }
                    case 2: {
                        String nameOfFile = getValueFromUser("Enter directory name");
                        fileManagerOption.createNewDirectory(nameOfFile);
                        break;
                    }
                    case 3: {
                        String nameOfFile = getValueFromUser("Enter name of the file or the directory should be deleted");
                        fileManagerOption.deleteFileOrDirectory(nameOfFile);
                        break;
                    }
                    case 4: {
                        String nameOfFile = getValueFromUser("Enter name of the directory");
                        fileManagerOption.showDirectoryContent(nameOfFile);
                        break;
                    }
                    case 5: {
                        String nameOfFile = getValueFromUser("Enter file name should be renamed");
                        String nameOfFile2 = getValueFromUser("Enter new file name");
                        fileManagerOption.renameFileOrDirectory(nameOfFile, nameOfFile2);

                        break;
                    }
                    case 6: {
                        String nameOfFile = getValueFromUser("Enter copied file name");
                        String nameOfFile2 = getValueFromUser("Enter new file name");
                        fileManagerOption.copyFileToAnotherDirectory(nameOfFile, nameOfFile2);
                        break;
                    }
                    case 7:
                        String nameOfFile = getValueFromUser("Enter the name of converting text file");
                        String nameOfFile2 = getValueFromUser("Enter the name of finished PDF-file");
                        fileManagerOption.convertTextFileToPdf(nameOfFile, nameOfFile2);
                }
            } catch (IOException x) {
                x.printStackTrace();
            }
        }
    }

    private static String getValueFromUser(String text) {
        System.out.println(text);
        try {
            text = READER.readLine();
        } catch (IOException x) {
            x.printStackTrace();
        }
        return text;
    }

    private static void fileManagerMenu() {
        System.out.println("Create a new file: enter 1\nCreate a new directory: enter 2\n" +
                "Delete direcroty or file: enter 3\nShow directory content: enter 4\n" +
                "Rename the file: enter 5\n" +
                "Copy the file to the another directory: enter 6\nConvert the text file to pdf-format: enter 7");
    }
}
