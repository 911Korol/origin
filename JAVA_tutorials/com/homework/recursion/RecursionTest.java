package com.homework.recursion;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;


/**
 * Created by 007 on 24.06.2017.
 */
public class RecursionTest {
    private RecursionExercises recursionExercises = new RecursionExercises();

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void testShouldReturnAmountOfPairedNumbersByRecursion() {
        System.out.println(recursionExercises.showAmountOfPairedNumber(332348, 0));
        Assert.assertEquals("Paired numbers is: 8\n" +
                                    "Paired numbers is: 4\n" +
                                    "Paired numbers is: 2\n3\r\n", systemOutRule.getLog());
    }

    @Test
    public void testShouldReturnCountOneNumbers() {
        System.out.println(String.format("The amount of number one is: " + recursionExercises.showCountOneNumbers(2135917181, 0)));
        Assert.assertEquals("The biggest unpaired number is : 9\n" +
                                    "The amount of number one is: 4", systemOutRule.getLog().trim());
//        System.out.println(System.getProperties());
    }
}
