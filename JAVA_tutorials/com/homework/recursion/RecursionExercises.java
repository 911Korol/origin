package com.homework.recursion;


public class RecursionExercises {
    private static int biggestUnpairedNumber = 0;

    public static void main(String[] args) {
        RecursionExercises recursionExercises = new RecursionExercises();
        System.out.println(String.format("The amount of number one is:%s", recursionExercises.showCountOneNumbers(21317181, 0)));
        System.out.println(String.format("The biggest unpaired number is :%s", biggestUnpairedNumber));
        System.out.println(String.format("Amount of paired number is: %s", recursionExercises.showAmountOfPairedNumber(1234567890, 0)));
    }


    public int showCountOneNumbers(int number, int countOne) {
        int mod = number % 10;
        if (mod == 1) {
            ++countOne;
        }

        if (mod > biggestUnpairedNumber && mod % 2 != 0) {
            biggestUnpairedNumber = mod;
        }

        if (number < 10) {
            System.out.println(String.format("The biggest unpaired number is : %s", biggestUnpairedNumber));
            return countOne;
        }
        return showCountOneNumbers(number / 10, countOne);
    }

    public int showAmountOfPairedNumber(int number, int countPaired) {
        int mod = number % 10;
        if (mod % 2 == 0) {
            System.out.printf("Paired numbers is: %s\n", mod);
            countPaired++;
        }
        if (number < 10) {
            return countPaired;
        }
        return showAmountOfPairedNumber(number / 10, countPaired);
    }
}
