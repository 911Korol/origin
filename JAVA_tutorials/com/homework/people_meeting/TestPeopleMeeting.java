package com.homework.people_meeting;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//import static com.homework.people_meeting.RegistrationForm.registreteUser;

/**
 * Создать приложение, позволяющие людям знакомиться.
 * Приложение должно позволять:
 * 1) Регистрироваться человеку старше 18 лет.=====================
 * 2) После регистрации выводить список подходящих мужчин/ женщин для этого человека по возрасту.=================
 * 3) Просматривать зарегистрировавшихся людей. Для мужчин выводить только женщин и наоборот.=====================
 * 4) Просматривать анкету отдельного человека(поиск по имени и фамилии)==========================================
 * 5) Организовать "умный поиск". Пользователь вводит требования(город, пол, возраст, количество детей) и  выводить людей,
 * которые соответствуют требованиям.====================================
 */
public class TestPeopleMeeting {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        User user;
        while (true) {
            System.out.println("Enter your sex(male/female)");
            Sex sex;
            try
            {sex = Sex.valueOf(reader.readLine().toUpperCase());}
            catch (IllegalArgumentException x){
                continue;
            }
            System.out.println("Please, enter your age");
            int age = Integer.parseInt(reader.readLine());
            if (age < 18) {
                System.out.println("Registration is only available to users over 18 years of age!");
                continue;
            }
            System.out.println("Enter your name");
            String name = reader.readLine();
            System.out.println("Enter your surname");
            String surname = reader.readLine();
            System.out.println("Enter your city of residence");
            String city = reader.readLine();
            System.out.println("How many children do you have?");
            int childrenAmount = Integer.parseInt(reader.readLine());
            user = new User(sex, name, surname, city, childrenAmount, age);
            break;
        }
        System.out.println(user);
        UserService userService = new UserService();
        userService.showPersonAccordingToRequarements();
        userService.showParticularUserByName("Igor");
    }

}



