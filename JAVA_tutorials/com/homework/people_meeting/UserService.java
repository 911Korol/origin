package com.homework.people_meeting;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by 007 on 02.05.2017.
 */
public class UserService {
    private User[] userList;

    public UserService() {

        userList = new User[]{
                new User(Sex.FEMALE, "Linda", "Lopes", "Kyiv", 1, 25),
                new User(Sex.FEMALE, "Lina", "Lesk", "Kyiv", 0, 23),
                new User(Sex.MALE, "Leon", "Hord", "Lviv", 1, 30),
                new User(Sex.MALE, "Igor", "Juk", "Kyiv", 1, 41),
                new User(Sex.FEMALE, "Katya", "Lorenso", "Odessa", 4, 35),
                new User(Sex.MALE, "Ivan", "Serp", "Dnipro", 2, 25),
                new User(Sex.MALE, "Kolya", "Smolar", "Lviv", 1, 45),
                new User(Sex.FEMALE, "Lida", "Lopes", "Kyiv", 1, 25),
                new User(Sex.FEMALE, "Lena", "Lesk", "Kyiv", 0, 23),
                new User(Sex.MALE, "Leonid", "Hord", "Lviv", 1, 30),
                new User(Sex.MALE, "Misha", "Juk", "Kyiv", 2, 21),
                new User(Sex.FEMALE, "Kat", "Lorenso", "Odessa", 4, 35),
                new User(Sex.MALE, "Igor", "Serp", "Dnipro", 3, 25),
                new User(Sex.MALE, "Andrey", "Smolar", "Lviv", 1, 55)
        };
    }

    public User[] getUserList() {
        return userList;
    }
    public void showPersonAccordingToRequarements() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        User particularUser;
        while (true) {
            System.out.println("Enter your sex(male/female)");
            Sex sex;
            try
            {sex = Sex.valueOf(reader.readLine().toUpperCase());}
            catch (IllegalArgumentException x){
                continue;
            }
            System.out.println("Please, enter your age");
            int age = Integer.parseInt(reader.readLine());
            if (age < 18) {
                System.out.println("Registration is only available to users over 18 years of age!");
                continue;
            }
            System.out.println("Enter your name");
            String name = reader.readLine();
            System.out.println("Enter your surname");
            String surname = reader.readLine();
            System.out.println("Enter your city of residence");
            String city = reader.readLine();
            System.out.println("How many children do you have?");
            int childrenAmount = Integer.parseInt(reader.readLine());
            particularUser = new User(sex, name, surname, city, childrenAmount, age);
            break;
        }
        System.out.println(particularUser);
        for (User user1 : userList) {
            if (particularUser.equals(user1)){
                System.out.println(particularUser);
            }
        }
    }
    public void showSuitablePersonByAgeAndSex(User user) {
        for (User user1 : userList) {
            if (user1.getSex() != user.getSex() && user1.getAge() == user.getAge()) {
                System.out.println(user1);
            }
        }
    }
    public void showParticularUserByName(String name){
        for (User user : userList) {
            if (user.getName().equals(name)){
                System.out.println(user);
            }
        }
    }
    public void showParticularUserBySurame(String surname){
        for (User user : userList) {
            if (user.getSurname().equals(surname)){
                System.out.println(user);
            }
        }
    }
}


