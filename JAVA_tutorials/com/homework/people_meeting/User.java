package com.homework.people_meeting;


public class User {
    private int age;
    private Sex sex;
    private String name;
    private String surname;
    private String city;
    private int amountOfChildren;


    public User(Sex sex, String name, String surname, String city, int amountOfChildren, int age) {
        this.age = age;
        this.sex = sex;
        this.name = name;
        this.city = city;
        this.surname = surname;
        this.amountOfChildren = amountOfChildren;
    }

    public User() {
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age <= 0) {
            age = 0;
        }
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getAmountOfChildren() {
        return amountOfChildren;
    }

    public void setAmountOfChildren(int amountOfChildren) {
        this.amountOfChildren = amountOfChildren;
    }

    @Override
    public String toString() {
        return "Name : " + name + '\n' + "Surname : " + surname + '\n' + "Sex: " + sex + '\n' + "Age: " + age +
                '\n' + "City: " + city + '\n' + "Children: " + amountOfChildren + '\n' + "_______________________";
    }

}

//domain -> DAO(Middle ware), service <-> database
