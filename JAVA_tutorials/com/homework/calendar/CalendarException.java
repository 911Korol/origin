package com.homework.calendar;

import java.time.DateTimeException;
import java.time.zone.ZoneRulesException;
import java.util.TimeZone;

/**
 * Created by 007 on 28.06.2017.
 */
public class CalendarException extends Exception{
    public CalendarException(String message){
        super(message);
    }


    public void printMessage(String timeZone) {
        System.out.println(String.format("You have entered incorrect data : %s", timeZone) + getMessage());
    }
}
