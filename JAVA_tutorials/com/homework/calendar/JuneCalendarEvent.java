package com.homework.calendar;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.Map;


public class JuneCalendarEvent {           //Почему класс зеленый???????
    private Map<LocalDate, String> event;

    public JuneCalendarEvent() {
        event = new LinkedHashMap<>();
        int monthNumber = 6;
        int yearNumber = 2017;
        event.put(LocalDate.of(yearNumber, monthNumber, 18), "Going to eat");
        event.put(LocalDate.of(yearNumber, monthNumber, 19), "Going to play soccer");
        event.put(LocalDate.of(yearNumber, monthNumber, 20), "Going to run");
        event.put(LocalDate.of(yearNumber, monthNumber, 21), "Going to create a good programme");
        event.put(LocalDate.of(yearNumber, monthNumber, 22), "Going to walk");
        event.put(LocalDate.of(yearNumber, monthNumber, 23), "Going to sleep");
        event.put(LocalDate.of(yearNumber, monthNumber, 24), "Going to do nothing");
        event.put(LocalDate.of(yearNumber, monthNumber, 25), "Going to watch TV");
        event.put(LocalDate.of(yearNumber, monthNumber, 26), "Going to study");
        event.put(LocalDate.of(yearNumber, monthNumber, 27), "Going to sing");
        event.put(LocalDate.of(yearNumber, monthNumber, 28), "Going to visit a cafe");
    }

    public Map<LocalDate, String> getEvent() {
        return event;
    }
}

