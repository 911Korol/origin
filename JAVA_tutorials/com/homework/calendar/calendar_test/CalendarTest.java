import com.homework.calendar.CalendarException;
import com.homework.calendar.CalendarOptions;
import com.homework.calendar.JuneCalendarEvent;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.cglib.core.Local;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

//Clean console?????
@RunWith(MockitoJUnitRunner.class)
public class CalendarTest {
    private JuneCalendarEvent juneCalendarEvent = new JuneCalendarEvent();
    private static final String LOS_ANGELES_TIME_ZONE = "America/Los_Angeles";
    private static final String LOCAL_TIME_ZONE = "Europe/Kiev";
    private static final String MADRID_TIME_ZONE = "Europe/Madrid";
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    private CalendarOptions calendarOptions = new CalendarOptions();
    private LocalDate localDate = LocalDate.now();

//    @Mock//17.05.19 01:51
//    CalendarOptions calendarOptions;
//
//    @Mock
//    LocalDate localDate;

//    @Mock
//    LocalTime localTime = Mockito.mock(LocalTime.class);

    @Mock
    LocalTime localTime;

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @BeforeClass
    public static void beforeAllClasses() {
        System.out.println("Before all classes");
    }

    @Test
    public void shouldReturnLocalTime() {
        calendarOptions.getLocalTime();
//        Mockito.when(localTime.get(LocalTime.now())).thenReturn(LocalTime.now());
        Mockito.when(LocalTime.now()).thenReturn(LocalTime.of(12, 12, 12));
        LocalTime localTime = LocalTime.now();//should be use mock the now method
        Assert.assertEquals("Current time is: " + localTime, systemOutRule.getLog().trim());
    }

    @Test
    public void shouldReturnLocalDate() {
        calendarOptions.getLocalDate();
        Assert.assertEquals("Current date is: " + localDate, systemOutRule.getLog().trim());
    }

    @Test
    public void shouldReturnDayOfWeek() {
        DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
        calendarOptions.getDayOfWeek();
        Assert.assertEquals("Today is: " + dayOfWeek, systemOutRule.getLog().trim());
    }

    @Test
    public void shouldReturnDayNumberOfYear() {
        int dayOfYear = LocalDate.now().getDayOfYear();
        calendarOptions.getDayNumberOfYear();
        Assert.assertEquals("Today is: " + dayOfYear + " day", systemOutRule.getLog().trim());
    }

    @Test
    public void shouldReturnAmountOfDaysToTheEndOfTheYear() {
        LocalDate localDate = LocalDate.now();
        int dayOfYear = localDate.getDayOfYear();
        int dayPerYear = Year.now().length();
        int remainingDay = dayPerYear - dayOfYear;
        calendarOptions.getAmountOfDaysToTheEndOfTheYear();
        Assert.assertEquals("Remaining days are " + remainingDay, systemOutRule.getLog().trim());
    }

    @Test
    public void shouldReturnEvent() {
        calendarOptions.getEvent(localDate);
        new Date(1234568789444L);//10-10-2017
        String expected = juneCalendarEvent.getEvent().get(localDate);//
        Assert.assertEquals(expected, systemOutRule.getLog().trim());
    }

    @Test
    public void shouldReturnParticularDateFormat() {
        String dateFormat = "y.M.d";
        dateTimeFormatter = DateTimeFormatter.ofPattern(dateFormat);
        calendarOptions.getParticularDateFormat(dateFormat);
        Assert.assertEquals(dateTimeFormatter.format(localDate), systemOutRule.getLog().trim());
    }

    @Test
    public void shouldReturnDateAfterYear() {
        localDate = LocalDate.now().plusYears(1);
        calendarOptions.getDateAfterYear();
        Assert.assertEquals("The date after week is " + localDate, systemOutRule.getLog().trim());
    }

    @Test
    public void shouldReturnDateAfterMonth() {
        localDate = LocalDate.now().plusMonths(1);
        calendarOptions.getDateAfterMonth();
        Assert.assertEquals("The date after month is " + localDate, systemOutRule.getLog().trim());
    }

    @Test
    public void shouldReturnDateAfterWeek() {
        localDate = LocalDate.now().plusWeeks(1);
        calendarOptions.getDateAfterWeek();
        Assert.assertEquals("", "The date after week is " + localDate, systemOutRule.getLog().trim());
    }

    @Test
    public void shouldReturnTimeAndDayOfWeekOfParticularTimeZone() throws CalendarException {
        dateTimeFormatter = DateTimeFormatter.ofPattern("VV, E, H:mm");
        String timeZoneData = dateTimeFormatter.format(ZonedDateTime.now(ZoneId.of(LOCAL_TIME_ZONE)));
//        calendarOptions.getTimeAndDayOfWeekOfParticularTimeZone(LOCAL_TIME_ZONE);
        Assert.assertEquals(timeZoneData, systemOutRule.getLog().trim());
    }

    @Test
    public void shouldReturnCurrentDateAndTimeOfThreeTimeZone()throws CalendarException {
        dateTimeFormatter = DateTimeFormatter.ofPattern("VV, d MMMM  yyyy H:mm ");
        String timeZoneData = dateTimeFormatter.format(ZonedDateTime.now(ZoneId.of(LOCAL_TIME_ZONE)));
//        calendarOptions.getTimeAndDayOfWeekOfParticularTimeZone(LOCAL_TIME_ZONE);
        Assert.assertEquals(timeZoneData, systemOutRule.getLog().trim());
    }
}
