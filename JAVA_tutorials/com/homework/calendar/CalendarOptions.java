package com.homework.calendar;


import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by 007 on 11.06.2017.
 */
public class CalendarOptions {
    private static JuneCalendarEvent event = new JuneCalendarEvent();
    private static final String LOS_ANGELES_TIME_ZONE = "America/Los_Angeles";
    private static final String LOCAL_TIME_ZONE = "Europe/Kiev";
    private static final String MADRID_TIME_ZONE = "Europe/Madrid";

    public void getCurrentDateAndTimeOfThreeTimeZone() {//Single responsibility??????????????
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("VV, d MMMM  yyyy H:mm ");
        String stringOfDayTimeZone = String.format("%search_for_pair_numbers\n%search_for_pair_numbers\n%search_for_pair_numbers",
                dateTimeFormatter.format(ZonedDateTime.now(ZoneId.of(LOCAL_TIME_ZONE))),
                dateTimeFormatter.format(ZonedDateTime.now(ZoneId.of(LOS_ANGELES_TIME_ZONE))),
                dateTimeFormatter.format(ZonedDateTime.now(ZoneId.of(MADRID_TIME_ZONE))));
        System.out.println(stringOfDayTimeZone);
        getEvent(LocalDate.now());
    }
// TODO: 29.06.2017  
//    public void getTimeAndDayOfWeekOfParticularTimeZone(String timeZone) {
//        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("VV, E, H:mm");
//        try {
////            ZoneId.getAvailableZoneIds().contains(timeZone);
//            String dayTime = dateTimeFormatter.format(ZonedDateTime.now(ZoneId.of(timeZone)));
//            System.out.println(dayTime);
//        }   catch (CalendarException e) {
//                e.printMessage(timeZone);
//            }
//        }
//    }
   /*public void getTimeAndDayOfWeekOfParticularTimeZone(String timeZone) throws CalendarException {
       DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("VV, E, H:mm");
       try {
           String dayTime = dateTimeFormatter.format(ZonedDateTime.now(ZoneId.of(timeZone)));
           System.out.println(dayTime);
       } catch (DateTimeException | NumberFormatException x) {//own type of exception
           throw new CalendarException(String.format("You have entered incorrect data : %s", timeZone)) {};
       }
   }*/

    public void getDateAfterWeek() {
        LocalDate dateAfterWeek = LocalDate.now().plusWeeks(1);
        System.out.println(String.format("The date after week is %s", dateAfterWeek));
    }

    public void getDateAfterMonth() {
        LocalDate dateAfterMonth = LocalDate.now().plusMonths(1);
        System.out.println(String.format("The date after month is %s", dateAfterMonth));
    }

    public void getDateAfterYear() {
        LocalDate dateAfterYear = LocalDate.now().plusYears(1);
        System.out.println(String.format("The date after week is %s", dateAfterYear));
    }

    public void getParticularDateFormat(String typeOfDateFormat) {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(typeOfDateFormat);
        String localDate = dateFormatter.format(LocalDate.now());
        System.out.println(localDate);
    }

    public void getLocalTime() {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalTime localTime = LocalTime.now();
        System.out.println(String.format("Current time is: %s", dateFormatter.format(localTime)));
    }

    public void getLocalDate() {
        LocalDate localDate = LocalDate.now();
        System.out.println(String.format("Current date is: %s", localDate));
    }

    public void getDayOfWeek() {
        DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
        System.out.println(String.format("Today is: %s", dayOfWeek));
    }

    public void getDayNumberOfYear() {
        int dayOfYear = LocalDate.now().getDayOfYear();
        System.out.println(String.format("Today is: %s", dayOfYear));
    }

    public void getAmountOfDaysToTheEndOfTheYear() {
        int dayOfYear = LocalDate.now().getDayOfYear();
        int dayPerYear = Year.now().length();
        int remainingDay = dayPerYear - dayOfYear;
        System.out.println(String.format("Remaining days are %s", remainingDay));
    }

    public void getEvent(LocalDate date) {
        System.out.println(event.getEvent().get(date));
    }

    public void removeEvent(LocalDate date, String newEvent) {
        Map<LocalDate, String> event = CalendarOptions.event.getEvent();
        /*for (Map.Entry<LocalDate, String> entry : event.entrySet()) {
            if (entry.getKey().equals(date)) {
                entry.setValue(newEvent);
            }
        }*/

        if (event.containsKey(date)) {
            event.put(date, newEvent);
        }
    }
}


