package com.homework.calendar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.Scanner;

/**
 * Created by 007 on 11.06.2017.
 */
public class CalendarMain {
    private static CalendarOptions calendarOptions = new CalendarOptions();
    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private static final String INCORRECT_DATA = "Please, enter correct data";
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        while (true) {
            menu();
            int menuSelection = Integer.parseInt(reader.readLine());
            switch (menuSelection) {
                case 1: {
                    calendarOptions.getCurrentDateAndTimeOfThreeTimeZone();
                    break;
                }
                case 2: {
                    System.out.println("Enter timezone using next format : Continent/City");
                    /*try {
                        calendarOptions.getTimeAndDayOfWeekOfParticularTimeZone(reader.readLine());
                    } catch (CalendarException e) {
                        e.printStackTrace();
                    }*/
                    continue;
                }
                case 3: {
                    calendarOptions.getDateAfterWeek();
                    continue;
                }
                case 4: {
                    calendarOptions.getDateAfterMonth();
                    continue;
                }
                case 5: {
                    calendarOptions.getDateAfterYear();
                    continue;
                }
                case 6: {
                    System.out.println("Enter preferable date format like: d M y or y.M.d ");
                    calendarOptions.getParticularDateFormat(reader.readLine());
                    continue;
                }
                case 7: {
                    calendarOptions.getEvent(getLocalDateFromUser());
                    break;
                }
                case 8: {
                    calendarOptions.removeEvent(getLocalDateFromUser(), getNewEventFromUser("Enter new event"));
                    break;
                }
                default: {
                    System.out.println(INCORRECT_DATA);
                    break;
                }
                case 9: {
                    advancedMenu();
                    int advancedMenuSelection = Integer.parseInt(reader.readLine());
                    switch (advancedMenuSelection) {
                        case 1: {
                            calendarOptions.getLocalTime();
                            break;
                        }
                        case 2: {
                            calendarOptions.getLocalDate();
                            break;
                        }
                        case 3: {
                            calendarOptions.getDayOfWeek();
                            break;
                        }
                        case 4: {
                            calendarOptions.getDayNumberOfYear();
                            break;
                        }
                        case 5: {
                            calendarOptions.getAmountOfDaysToTheEndOfTheYear();
                            break;
                        }
                        default: {
                            System.out.println(INCORRECT_DATA);
                            break;
                        }
                    }

                }
            }
        }
    }


    private static void menu() {
        System.out.println("Enter 1 to get current time of your location, Los Angeles and Madrid time\n" +
                "Enter 2 to get time and day of particular area\n" +
                "Enter 3 to know date after week\n" +
                "Enter 4 to know date after month\n" +
                "Enter 5 to know date after year\n" +
                "Enter 6 to get date format according to your preference\n" +
                "Enter 7 to get event of particular day\n" +
                "Enter 8 to remove event\n" +
                "Enter 9 to get advanced menu");
    }

    private static void advancedMenu() {
        System.out.println("Enter 1 to get local time\n" +
                "Enter 2 to get local date\n" +
                "Enter 3 to get the day of week\n" +
                "Enter 4 to get the day number of year\n" +
                "Enter 5 to get the amount of days to the end of the year");
    }

    private static LocalDate getLocalDateFromUser() throws IOException {
        System.out.println("Please enter necessary year like: 2017");
        int year = scanner.nextInt();
        System.out.println("Please enter necessary month like: 12");
        int month = new Scanner(System.in).nextInt();
        System.out.println("Please enter necessary day like: 30");
        int day = Integer.parseInt(reader.readLine());
        LocalDate currentDate = LocalDate.of(year, month, day);
        System.out.println(currentDate);
        return currentDate;
    }

    private static String getNewEventFromUser(String message) {
        System.out.println("Enter new event");
        return scanner.nextLine();
    }
}
