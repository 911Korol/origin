package com.homework.inheritance_first_task.writer;


import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Created by 007 on 10.05.2017.
 */
public class WriterToFile extends Writer {
    @Override
    public void write(String text) throws IOException {
        Path newTextFile = Paths.get("./Poem.txt");
        text = modifyText(text);
        try (BufferedWriter writer = Files.newBufferedWriter(newTextFile, StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING)) {
            writer.write(text);
        } catch (IOException x) {
        }
    }
}
