package com.homework.inheritance_first_task.writer;

/**
 * Created by 007 on 10.05.2017.
 */
public abstract class Writer implements Writable {
    public String modifyText(String text) {
        return text.concat(" I'm ready for writting to file");
    }

}
