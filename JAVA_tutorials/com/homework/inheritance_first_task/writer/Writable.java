package com.homework.inheritance_first_task.writer;


import java.io.IOException;

/**
 * Created by 007 on 10.05.2017.
 */
public interface Writable {
    void write(String text) throws IOException;
}
