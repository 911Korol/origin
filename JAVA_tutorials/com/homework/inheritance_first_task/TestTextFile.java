package com.homework.inheritance_first_task;

import com.homework.inheritance_first_task.reader.ReaderFromFile;
import com.homework.inheritance_first_task.writer.Writer;
import com.homework.inheritance_first_task.writer.WriterToFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by 007 on 11.05.2017.
 */
public class TestTextFile {

    public static void main(String[] args) throws IOException {
        WriterToFile writerToFile = new WriterToFile();
        ReaderFromFile readerFromFile = new ReaderFromFile();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileText = reader.readLine();
        writerToFile.write(fileText);
        readerFromFile.read();
    }

}
