package com.homework.inheritance_first_task.reader;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by 007 on 11.05.2017.
 */
public interface Readable {
    void read() throws IOException;
}
