package com.homework.inheritance_first_task.reader;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

/**
 * Created by 007 on 11.05.2017.
 */
public class ReaderFromFile extends Reader {
    @Override
    public void read() throws IOException {
        String text;
        String line = "";
        try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get("./Poem.txt"))) {
            while ((text = bufferedReader.readLine()) != null) {
                line += text;
            }
            System.out.println(modifyText(line));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

