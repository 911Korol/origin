package com.homework.inheritance_first_task.reader;


/**
 * Created by 007 on 11.05.2017.
 */
public abstract class Reader implements Readable {
    public String modifyText(String text) {
        return text.replace("I'm ready for writting to file", " I'm from file");
    }
}
