package com.homework.arrayList_immitation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Видимость массива из другого метода????
 */

/*Создать имитацию коллекции ArrayList на основе массива для работы с типами int.
Класс должен выполнять следующие операции:

1) добавление элементов.
2) изменение/удаление элементов по индексу.
3) увеличение листа на заданное количество элементов.
4) уменьшение листа до заданного количество элементов.
5) вывод элементов в консоль в прямом и обратном порядке.
6) сортировка листа.
Все действия юзера записывать в файл с название Logs.txt в  формате дата(классы по работе с датами из Java 8).
По запросу юзера выводить в консоль данные файла Logs.txt


 */
public class TestArrayListImmitation {
    private static final BufferedReader BUFFERED_READER = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {
        ImitationArrayList list = new ImitationArrayList(10);
        while (true) {
            menu();
            int menuSelection = Integer.parseInt(BUFFERED_READER.readLine());
            try {
                switch (menuSelection) {
                    case 1: {
                        /*System.out.println("Input the value of new element");
                        list.addElement(Integer.parseInt(BUFFERED_READER.readLine()));*/
                        int newValue = getValueFromUser("Input the value of new element");
                        list.addElement(newValue);
                        break;
                    }
                    case 2: {
                        /*System.out.println("Input the value of increasing of array");
                        list.increaseCapacity(Integer.parseInt(BUFFERED_READER.readLine()));*/
                        int newValue = getValueFromUser("Input the value of increasing of array");
                        list.increaseCapacity(newValue);
                        break;
                    }
                    case 3: {
                        int newValue = getValueFromUser("Input the value of reducing of array");
                        list.reduceCapacity(newValue);
                        break;
                    }
                    case 4: {
                        list.printArr();
                        break;
                    }
                    case 5: {
                        list.printArrInverse();
                        break;
                    }
                    case 6: {
                        list.sortArray();
                        break;
                    }
                    case 7: {
                        list.showAllLogFile();
                        break;
                    }
                    case 8: {
                        String methodName = getNameOfMethod("Input the name of method: addElement, " +
                                "increaseCapacity, reduceCapacity, printArr, printArrInverse, sort ");
                        list.showDesiredMethod(methodName);
                        break;
                    }
                }
            } catch (Exception x) {
                System.out.println(x.getMessage());
            }
        }
    }

    private static int getValueFromUser(String text) throws IOException {
        System.out.println(text);
        return Integer.parseInt(BUFFERED_READER.readLine());
    }

    private static String getNameOfMethod(String text) throws IOException {
        System.out.println(text);
        return BUFFERED_READER.readLine();
    }

    private static void menu() {
        System.out.println("Добавить элемент в массив: нажните 1\n" +
                "Увеличить лист на заданное количество элементов: нажмите 2\n" +
                "Уменьшить лист до заданного количество элементов: нажмите 3\n" +
                "Вывод элементов в консоль в прямом порядке: 4\n" +
                "Вывод элементов в консоль в обратном порядке: 5\n" +
                "Сортировка листа: нажмите 6\n" +
                "Вывести журнал событий на экран: нажмите 7\n" +
                "Вывести журнал событий по методам: нажмите 8");
    }
}


