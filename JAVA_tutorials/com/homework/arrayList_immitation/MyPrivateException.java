package com.homework.arrayList_immitation;


import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;

/**
 * Created by 007 on 17.05.2017.
 */
public class MyPrivateException extends IOException {

    public MyPrivateException(String message) {
        super(message);
    }

    public void logException() throws IOException {
        Path exeption = Paths.get("./log/logException.txt");
        try (BufferedWriter writer = Files.newBufferedWriter(exeption, StandardOpenOption.CREATE, StandardOpenOption.APPEND);) {
            writer.write(new Date() + " " + getMessage() + "\n");
        }
    }
}
