package com.homework.arrayList_immitation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.Arrays;
import java.util.Date;

import static java.lang.System.out;

/**
 * Created by 007 on 28.04.2017.
 */
public class ImitationArrayList {
    private int[] intArray;
    private Date date;
    public static Path logFile = Paths.get("./log/log.txt");

    public ImitationArrayList(int size) {
        intArray = new int[size];
    }

    public void addElement(int newValue) {
        try {
            if (newValue == 0) {
                throw new MyPrivateException("Value is 0");
            }
            date = new Date();
            String log = date + " addElement -> " + newValue;
            writeLog(log);
            reSize();
            for (int i = 0; i < intArray.length; i++) {
                if (intArray[i] == 0) {
                    intArray[i] = newValue;
                    break;
                }
            }
        } catch (MyPrivateException x) {
            try {
                x.logException();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void reSize() {
        boolean shouldReSize = false;

        for (int i = 0; i < intArray.length; i++) {
            if (intArray[i] == 0) {
                shouldReSize = true;
                break;
            }
        }

        if (!shouldReSize) {
            int[] temp = new int[intArray.length * 2];

            for (int i = 0; i < intArray.length; i++) {
                temp[i] = intArray[i];
            }

            intArray = temp;
        }
    }

    public void increaseCapacity(int increasingNumber) {
        Date date = new Date();
        String log = date + " increaseCapacity -> " + increasingNumber;
        try {
            writeLog(log);
            intArray = Arrays.copyOf(intArray, intArray.length + increasingNumber);
        } catch (IOException x) {
            x.printStackTrace();
        }
    }

    public void reduceCapacity(int reducingNumber) {
        try {
            if (reducingNumber < 0) {
                throw new MyPrivateException("Value is less 0");
            }
            Date date = new Date();
            String log = date + " reduceCapacity -> " + reducingNumber;
            writeLog(log);
            intArray = Arrays.copyOf(intArray, intArray.length - reducingNumber);
        } catch (MyPrivateException x) {
            try {
                x.logException();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printArr() {
        Date date = new Date();
        String log = date + " printArr";
        try {
            writeLog(log);
        } catch (IOException x) {
            x.printStackTrace();
        }
        for (int element : intArray) {
            out.print(element);
        }
    }

    public void printArrInverse() {
        Date date = new Date();
        String log = date + " printArrInverse";
        try {
            writeLog(log);
        } catch (IOException x) {
            x.printStackTrace();
        }
        for (int i = intArray.length - 1; i >= 0; i--) {
            System.out.print(intArray[i] + " ");
        }
    }

    public void sortArray() {
        Date date = new Date();
        String log = date + " sort";
        try {
            writeLog(log);
        } catch (IOException x) {
            x.printStackTrace();
        }
        for (int i = 0; i < intArray.length - 1; i++) {
            for (int j = 0; j < intArray.length - 1 - i; j++) {
                if (intArray[j] > intArray[j + 1]) {
                    int a = intArray[j];
                    intArray[j] = intArray[j + 1];
                    intArray[j + 1] = a;
                }
            }
        }
        printArr();
    }

    private void writeLog(String log) throws IOException {
        try (BufferedWriter writer = Files.newBufferedWriter(logFile, StandardOpenOption.CREATE, StandardOpenOption.APPEND)) {
            writer.write("\n" + log);
        }
    }

    public void showAllLogFile() {
        try {
            BufferedReader reader = Files.newBufferedReader(logFile, Charset.defaultCharset());
            String lineFromFile;
            while ((lineFromFile = reader.readLine()) != null) {
                System.out.println(lineFromFile);
            }
        } catch (IOException x) {
            x.printStackTrace();
        }
    }

    public void showDesiredMethod(String desireMethodName) throws IOException {
        BufferedReader reader = Files.newBufferedReader(logFile, Charset.defaultCharset());
        String lineFromFile;
        while ((lineFromFile = reader.readLine()) != null) {
            if (lineFromFile.contains(desireMethodName)) {
                System.out.println(lineFromFile);
            }
        }
        System.out.println("------------------------");
    }

    public int[] getIntArray() {
        return intArray;
    }
}