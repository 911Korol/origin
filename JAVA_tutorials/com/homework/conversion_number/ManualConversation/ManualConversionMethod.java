package com.homework.conversion_number.ManualConversation;

/**
 * Created by 007 on 29.04.2017.
 */
public class ManualConversionMethod {
    public void conversionTenToTwo(int particularNumberToConversion) {
        String binarNaumber = "";
        while (true) {
            if (particularNumberToConversion % 2 == 0) {
                binarNaumber += 0;
            } else {
                binarNaumber += 1;
            }
            particularNumberToConversion /= 2;
            if (particularNumberToConversion == 1) {
                binarNaumber += 1;
                break;
            }
        }
        long result = Long.parseLong(String.valueOf(new StringBuilder(binarNaumber).reverse()));
        System.out.println(result);
    }
}