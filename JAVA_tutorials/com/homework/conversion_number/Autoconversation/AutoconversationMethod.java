package com.homework.conversion_number.Autoconversation;

/**
 * Created by 007 on 30.04.2017.
 */
public class AutoconversationMethod {
    public void converseNumbers(int x) {
        String binarConversion = Integer.toBinaryString(x);
        String octalConversion = Integer.toOctalString(x);
        String hexConversion = Integer.toHexString(x);
        System.out.println(String.format("Число %d в бинарной системе исчисления равно %search_for_pair_numbers, в восьмиричной %search_for_pair_numbers, в шестнадцатиричной- %search_for_pair_numbers",
                x, binarConversion, octalConversion, hexConversion));
    }
}
