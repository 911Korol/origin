package com.calculator;

import java.util.Random;

/**
 * Created by 007 on 08.06.2017.
 */
public class CalculatorOptions {

    public static final Random RANDOM = new Random();

    public int multiply() {
        return getFirstOperator() * getSecondOperator();
    }

    public int divide() {
        return getFirstOperator() / getSecondOperator();
    }

    public int summarize() {
        return getFirstOperator() + getSecondOperator();
    }

    public int subtract() {
        return getFirstOperator() - getSecondOperator();
    }

    public int getFirstOperator() {
        return RANDOM.nextInt(20);
    }

    public int getSecondOperator() {
        return RANDOM.nextInt(20);
    }

}
