package com.lessons.learn_collection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by 007 on 29.06.2017.
 */
public class CollectionLearning {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        list.add(10);
        list.add(5);
        list.add(22);
        list.add(15);
        list.add(10);
        list.add(15);

        Set<Integer> set = new HashSet<>(list);

        list = new ArrayList<>(set);

        System.out.println(list);

    }
}
