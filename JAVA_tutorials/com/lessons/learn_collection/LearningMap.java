package com.lessons.learn_collection;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 007 on 29.06.2017.
 */
public class LearningMap {
    private String name;
    private int count;

    public LearningMap(String name, int count) {
        this.name = name;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LearningMap that = (LearningMap) o;

        if (count != that.count) return false;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + count;
        return result;
//        return 2;
    }

    public static void main(String[] args) {

        LearningMap map1 = new LearningMap("Box",12);
        LearningMap map2 = new LearningMap("Box",12);
        Map<LearningMap, String> map = new HashMap<>();
//        if (e.hash == hash && (e.key == key || key.equals(e.key)))
// hashcode and equals aren't overriden - will added two objects
// hashcode - override and equals not overriden - will added two objects
// hashcode - override and equals overriden - will added one object


        map.put(null, "Hello1");
        map.put(null, "Hello2");
//        map.entrySet().forEach(entry -> System.out.println(entry.getKey().getName() + " : " + entry.getKey().getCount()));
        map.entrySet().forEach(entry -> System.out.println(entry.getValue()));
//        System.out.println(map1.hashCode());
    }
}
