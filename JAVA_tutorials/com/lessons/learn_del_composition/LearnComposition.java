package com.lessons.learn_del_composition;

/**
 * Created by 007 on 29.04.2017.
 */
public class LearnComposition {
    private Object object;

    public LearnComposition(Object object) {//agregation
        this.object = object;
    }
}

class LearnComposition1 {
    private Object object;

    public LearnComposition1() {
        this.object = new Object();//composition
    }
}
