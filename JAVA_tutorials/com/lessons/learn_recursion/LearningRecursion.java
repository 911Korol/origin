package com.lessons.learn_recursion;

import java.util.stream.IntStream;

/**
 * Created by 007 on 17.06.2017.
 */
public class LearningRecursion {
    static int[] intArray = new int[10];
    static {
        for (int i = 0; i <intArray.length ; i++) {
            intArray[i] = i;
        }

        IntStream.range(0, 10).forEach(i -> intArray[i] = i);
    }
    public static void printArrayByRecursion(int index){
        if (index == 0) {
            return;
        }

        System.out.print(intArray[index - 1] + " ");
        printArrayByRecursion(index - 1);
    }

    public static void getAllNumbersBetweenTwoNumbers(int a, int b) {
        if (a == b) {
            System.out.print(a + " ");
            return;
        }
        System.out.print(a + " ");
        getAllNumbersBetweenTwoNumbers(++a, b);
    }

    public static long calculateFactorial(int n) {
        if (n == 1) {
//            return 1;
        }
        if (n == 2) {
//            return 2;
        }
        return calculateFactorial(n - 1) * n;
    }

//    param = 6: calculateFactorial(5) * 6
//    param = 5: calculateFactorial(4) * 5 ->
//    param = 4: calculateFactorial(3) * 4 -> 24
//    param = 3: calculateFactorial(2) * 3 -> 6

    public static long gcd(long a, long b) {
        if (b == 0)
            return a;
        else
            return gcd(b, a % b);
    }


    public static void main(String[] args) {
//        printArrayByRecursion(10);
//        getAllNumbersBetweenTwoNumbers(10, 30);
//        System.out.println(calculateFactorial(1000));
        System.out.println(gcd(24, gcd(12, 18)));
    }
}
