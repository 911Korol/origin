package com.lessons.learn_switch;

/**
 * Created by 007 on 11.05.2017.
 */
public class LearnSwitch {
    public static void main(String[] args) {
        int a = 10;

        /*if (a == 10) {
            System.out.println("a == 10");
        } else if(a == 20) {
            System.out.println("a == 20");
        } else if(a == 30) {
            System.out.println("a == 30");
        } else if(a == 40) {
            System.out.println("a == 40");
        }*/

        switch (a) {
            case 10:
                System.out.println("a == 10");
                break;
            case 20:
                System.out.println("a == 20");
                break;
            case 30:
                System.out.println("a == 30");
                break;
            case 40:
                System.out.println("a == 40");
                break;
            default:
                System.out.println("Something else");
        }
    }
}
