package com.lessons.learning_inhenritance;

/**
 * Created by 007 on 09.05.2017.
 */
public class Child extends Parent {

    static {
        System.out.println("Static block initialization Child");//4
    }

    {
        System.out.println("Non - static block initialization Child");//5
    }

    public Child() {
        System.out.println("Constructor Child");//6
    }

    public void print() {
        System.out.println();
    }

    public void show() {
        Animal animal = new Animal();
        animal.print();
    }

}


//142356