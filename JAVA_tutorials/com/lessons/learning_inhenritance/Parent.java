package com.lessons.learning_inhenritance;

/**
 * Created by 007 on 09.05.2017.
 */
public class Parent implements Ballable {

    static {
        System.out.println("Static block initialization Parent");//1
    }

    {
        System.out.println("Non - static block initialization Parent");//2
    }

    public Parent() {
        System.out.println("Constructor Parent");//3
    }

    @Override
    public int getNumber() {
        return 0;
    }
}
