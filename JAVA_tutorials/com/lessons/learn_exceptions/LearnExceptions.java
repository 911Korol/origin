package com.lessons.learn_exceptions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLData;
import java.sql.SQLDataException;

/**
 * Created by 007 on 16.05.2017.
 */
public class LearnExceptions {
    public static void print() throws Exception {
        try {
            throw new IOException();
//            System.out.println();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            e = new SQLDataException();
            throw e;
//            System.out.println(e.getMessage());
        } finally {
//            System.out.println("Finally");
        }
    }

    public static int getNumber() {
        try {
            return 5;
        } finally {
            return 7;
        }
    }

    public void showException() throws MishaException {
        try {
            throw new IOException();
        } finally {
            throw new MishaException();
        }
    }

    public static void show() throws IOException {
        throw new IOException();
    }

    public static void print(String s) throws IOException {
        throw new IOException();
//            System.out.println();
    }

    public static void print(int a) throws IOException {
        try {
            if (1 == 1) {
                throw new IOException();
            } else {
                throw new SQLDataException();
            }
        } catch (IOException | SQLDataException e) {
            e.printStackTrace();
        }


    }

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            show();
            String s = reader.readLine();
            print();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

class MishaException extends IOException {

}
