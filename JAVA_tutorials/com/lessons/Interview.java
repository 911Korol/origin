package com.lessons;

/**
 * Created by 007 on 12.06.2017.
 */
public class Interview {
    public void processing(Book book) {//(Reference book)
//        do something with book
    }

    public void processing(String author1, String author2,
                           String author3, String author4,
                           int startPage, int endPage,
                           String nameArticle,
                           Boolean isUpdated, int year, int volume) {
//        do something with article

//        Заменить параметры на класс Reference, предложить рефакторинг
    }
}

class Reference {

    private String author1;
    private String author2;
    private String author3;
    private String author4;
    private boolean isUpdated;
    private int year;
    private int volume;

    public Reference(String author1, String author2, String author3, String author4, boolean isUpdated, int year, int volume) {
        this.author1 = author1;
        this.author2 = author2;
        this.author3 = author3;
        this.author4 = author4;
        this.isUpdated = isUpdated;
        this.year = year;
        this.volume = volume;

        for (int i = 0; i < 5; i++) {
            class Fo {

            }
        }
    }


}

class Book extends Reference {
    private int countOfpages;
    private String nameBook;

    public Book(String author1, String author2, String author3, String author4, boolean isUpdated, int year, int volume,
                int countOfpages, String nameBook) {
        super(author1, author2, author3, author4, isUpdated, year, volume);
        this.countOfpages = countOfpages;
        this.nameBook = nameBook;
    }
}


class Outer {

    class Inner {
        public final static String string = "";
    }

    public static void main(String[] args) {
        Outer outer = new Outer();
        Inner inner = outer.new Inner();
        String string = inner.string;
    }
}

class SubInner extends Outer.Inner {

    SubInner(Outer outer) {
        outer.super();
    }
}