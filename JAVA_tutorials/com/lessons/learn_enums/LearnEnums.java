package com.lessons.learn_enums;

import com.homework.people_meeting.Sex;

/**
 * Created by 007 on 02.05.2017.
 */
public class LearnEnums {
    public static void main(String[] args) {
        Sex sex = Sex.FEMALE;

        String sexString = "MaLe";

        Sex sex1 = Sex.valueOf(sexString.toUpperCase());
    }
}
