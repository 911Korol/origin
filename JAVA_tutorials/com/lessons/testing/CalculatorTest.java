import com.lessons.for_testing.Calculator;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.*;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by 007 on 11.05.2017.
 */
//@RunWith(JUnitParamsRunner.class)
@RunWith(JUnitParamsRunner.class)
public class CalculatorTest {

    private Calculator calculator;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();


    @BeforeClass
    public static void beforeAllTests() {
//        System.out.println("Before all tests");
    }

    @AfterClass
    public static void afterAllTests() {
//        System.out.println("After all tests");
    }

    @Before
    public void beforeEachTest() {
//        System.out.println("Before each");
        calculator = new Calculator();
    }

    @After
    public void afterEachTest() {
//        System.out.println("After each");
        calculator = null;
    }

    //    @Ignore
    @Test
    public void shouldReturnMultipliedValues() {
        Assert.assertEquals("Should return 16", 16, calculator.multiply(4, 4));
    }

    @Test
    public void shouldReturnMinusValues() {
        Assert.assertEquals("Should return 4", 4, calculator.minus(8, 4));
    }

    /*@Parameterized.Parameter(value = 1)
    public int valueOne;

    @Parameterized.Parameter(value = 0)
    public int valueTwo;*/


    /*@Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{{8, 2}, {7, 9}};
        return Arrays.asList(data);
    }*/


    /*@Test
    public void shouldReturnMinusValuesParam() {
        int result = valueOne - valueTwo;
        Assert.assertEquals("Should return " + result, valueOne - valueTwo, calculator.minus(valueOne, valueTwo));
    }*/


    @Test
    @Parameters({"8|9", "2|5"})
    public void shouldReturnResultAfterMultiplyOperationParam(int valueOne, int valueTwo) {
        assertEquals("Should return multiply values", valueOne * valueTwo,
                calculator.multiply(valueOne, valueTwo));
    }


    @Test(expected = NullPointerException.class)
    public void shouldThrownException() {
        calculator.thrownException();
    }


    @Test
    public void shouldThrownExceptionWithRule() {
        exception.expect(NullPointerException.class);
        exception.expectMessage("NPE");
        calculator.thrownException();
    }


    @Test
    public void checkOutput() {
        calculator.print();
        Assert.assertEquals("Should print\"Hello, ArrayList\"", "Hello, ArrayList", systemOutRule.getLog());
    }

}