package spying;

import com.lessons.for_testing.Door;
import com.lessons.for_testing.Engine;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

/**
 * Created by 007 on 31.05.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class SpyingSample {

    @Spy
    Engine spyEngine = new Engine();

/*
    @Mock
    Engine mockEngine = new Engine();
*/

    @Spy
    Door spyDoor = new Door();

    @Test
    public void testSpy() {
        Mockito.doReturn(45).when(spyEngine).getNumberOfCylinders();

        Assert.assertEquals(spyEngine.getNumberOfCylinders(), 45);
    }

    //    Stub void method Using doThrow
//    @Test(expected = NullPointerException.class)
    @Test(expected = IOException.class)
    public void testThrowException() {
        Mockito.doThrow(IOException.class).when(spyDoor).shouldThrowException();
        spyDoor.shouldThrowException();
    }

    //    Stub void method Using doNothing
    @Test
    public void testDoNothing() {
        Mockito.doNothing().when(spyDoor).testDoNothing();
        Assert.assertEquals(spyDoor.getNumberOfDoor(), 4);
    }

}
