import com.homework.file_manager.FileManagerOption;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Created by 007 on 31.05.2017.
 */
public class FileManagerTest {
    private FileManagerOption fileManagerOption = new FileManagerOption();
    private String NAME_OF_DIRECTORY = "./TestLog";
    private String NAME_OF_FILE = "./Test.txt";
    private Path PATH_TO_DIR = Paths.get(NAME_OF_DIRECTORY);
    private Path PATH_TO_FILE = Paths.get(NAME_OF_FILE);


    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();


    @Test
    public void shouldCreateNewFile() throws IOException {
        fileManagerOption.createNewFile(NAME_OF_FILE);
        Assert.assertEquals(true, Files.exists(PATH_TO_FILE));
        Files.delete(PATH_TO_FILE);
    }

    @Test
    public void shouldCreateNewDirectory() throws IOException {
        fileManagerOption.createNewDirectory(NAME_OF_DIRECTORY);
        Assert.assertEquals(true, Files.exists(PATH_TO_DIR));
        Files.delete(PATH_TO_DIR);
    }

    @Test
    public void shoulDeleteFileOrDirectory() {
        fileManagerOption.createNewDirectory(NAME_OF_FILE);
        fileManagerOption.deleteFileOrDirectory(NAME_OF_FILE);
        Assert.assertEquals(false, Files.exists(PATH_TO_DIR));
    }

    @Test
    public void shouldRenameFileOrDirectory() throws IOException {
        fileManagerOption.createNewDirectory(NAME_OF_DIRECTORY);
        Path newNamePath = Paths.get(NAME_OF_FILE + "qqq");
        fileManagerOption.renameFileOrDirectory(NAME_OF_DIRECTORY, (newNamePath.toString()));
        Assert.assertEquals(true, Files.exists(newNamePath));
        Files.delete(newNamePath);
    }

    @Test
    public void shouldShowDirectoryContent() throws IOException {
        fileManagerOption.createNewDirectory(NAME_OF_DIRECTORY);
        Path newFile = Paths.get(NAME_OF_DIRECTORY + "/Test.txt");
        fileManagerOption.createNewFile(newFile.toString());
        String[] ar = (String[]) Files.list(PATH_TO_DIR).toArray();
        String s = "";
        for (String o : ar) {
            s += o;
        }
        fileManagerOption.showDirectoryContent(NAME_OF_DIRECTORY);
        Assert.assertEquals(systemOutRule.getLog(), s);
        Files.delete(newFile);
        Files.delete(PATH_TO_DIR);
    }

    @Test
    public void shouldCopyFileToAnotherDir() {
        fileManagerOption.createNewFile(NAME_OF_FILE);
        String testString = "Testing String should show correcting process of method";

        try (BufferedWriter writer = Files.newBufferedWriter(PATH_TO_FILE, StandardOpenOption.CREATE, StandardOpenOption.APPEND)) {
            writer.write(testString);
        } catch (IOException x) {
            x.printStackTrace();
        }
        fileManagerOption.copyFileToAnotherDirectory(NAME_OF_FILE, NAME_OF_FILE + "new");
        Path testPath = Paths.get(NAME_OF_FILE + "new");
        try (BufferedReader reader = Files.newBufferedReader(testPath, Charset.defaultCharset())) {
            String fileText;
            while ((fileText = reader.readLine()) != null) {
                System.out.println(fileText);
            }
            Files.delete(testPath);
        } catch (IOException x) {
            x.printStackTrace();
        }
        Assert.assertEquals(true, systemOutRule.getLog().contains(testString));
    }

    @Test
    public void shouldConvertTextFileToPdf() {
        fileManagerOption.createNewFile(NAME_OF_FILE);
        String testText = "This text is testing";
        String pdfText = "";
        try {
            BufferedWriter writer = Files.newBufferedWriter(PATH_TO_FILE, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            writer.write(testText);
            writer.close();
            Path pdfFileName = Paths.get("./Test.pdf");
            fileManagerOption.convertTextFileToPdf(NAME_OF_FILE, pdfFileName.toString());
            PdfReader pdfReader = new PdfReader(pdfFileName.toString());
            pdfText = PdfTextExtractor.getTextFromPage(pdfReader, 1);
            pdfReader.close();
            Files.delete(PATH_TO_FILE);
            Files.delete(pdfFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(testText, pdfText);
    }
}
