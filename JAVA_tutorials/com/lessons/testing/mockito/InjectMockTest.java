package mockito;

import com.lessons.for_testing.Car;
import com.lessons.for_testing.Door;
import com.lessons.for_testing.Engine;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Created by 007 on 28.05.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class InjectMockTest {

    @Mock
    Door door;

    @Mock
    Engine engine;

    @InjectMocks
    Car carMock;


    @Test
    public void testInjectMock() {
        Door carDoor = carMock.getDoor();
        Engine carEngine = carMock.getEngine();

        Mockito.when(carDoor.getNumberOfDoor()).thenReturn(8);
        Mockito.when(carEngine.getNumberOfCylinders()).thenReturn(9);


        Assert.assertEquals(carDoor.getNumberOfDoor(), 8);
        Assert.assertEquals(carEngine.getNumberOfCylinders(), 9);
    }


    @Test(expected = IOException.class)
    public void testThrowException() {
        Mockito.doThrow(IOException.class).when(door).shouldThrowException();
        door.shouldThrowException();
    }

    @Test
    public void testConsecutiveCalls() {
        Mockito.when(door.getNumberOfDoor()).thenReturn(8, 12);
      /*  Assert.assertEquals(door.getNumberOfDoor(), 8);
        Assert.assertEquals(door.getNumberOfDoor(), 12);*/

//        Mockito.verify(door, Mockito.times(2)).getNumberOfDoor();
//        Mockito.verify(door, Mockito.atLeastOnce()).getNumberOfDoor();
//        Mockito.verify(door, Mockito.atMost(2)).getNumberOfDoor();
//        Mockito.verify(door, Mockito.never()).getNumberOfDoor();
    }

    @Test
    public void tryMatchers() {
        int number = 5;
        int anotherNumber = 10;
        String text = "Hello";
//        engine.plusStringWithNumber(text, number);
        engine.plusStringWithNumber(text, anotherNumber);
//        verify(engine).plusStringWithNumber(anyString(), anyInt());
//        verify(engine).plusStringWithNumber(anyString(), eq(anotherNumber));
//        verify(engine).plusStringWithNumber(eq("He"), eq(anotherNumber));
    }
}
