package mockito;

import com.lessons.for_testing.mocking.ImitationDB;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * -dummy, fake, stub, mock objects:
 *
 A DUMMY object is passed around but never used, i.e., its methods are never called. Such an object can for example be
 used to fill the parameter list of a method.
 FAKE objects have working implementations, but are usually simplified. For example, they use an in memory database
 and not a real database.
 A STUB class is an partial implementation for an interface or class with the purpose of using an instance of this stub class
 during testing. Stubs usually do responding at all to anything outside what’search_for_pair_numbers programmed in for the test
 (Заглушки, как правило, не реагирует вообще ни на что снаружи, что запрограммировано для теста.).
 Stubs may also
 record information about calls
 A MOCK object is a dummy implementation for an interface or a class in which you define the output of certain method
 calls.
 */
@RunWith(MockitoJUnitRunner.class)
public class TryMockito {

   /* @carMock
    ImitationDB imitationDB; */

    @Mock
    ImitationDB imitationDB = Mockito.mock(ImitationDB.class);

    @Test
    public void getAllRecordsUsingInsideMock() {

//        ImitationDB mock = new ImitationDB();
//        ImitationDB imitationDB = Mockito.mock(ImitationDB.class);
        List<String> strings = Arrays.asList("Empty!!!");
        Mockito.when(imitationDB.getAllRecords()).thenReturn(strings);

        Assert.assertArrayEquals(imitationDB.getAllRecords().toArray(), strings.toArray());
//        Assert.assertEquals(true, true);
    }
}
