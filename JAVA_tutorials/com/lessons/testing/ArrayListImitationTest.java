import com.homework.arrayList_immitation.ImitationArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.*;
import org.junit.contrib.java.lang.system.SystemOutRule;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class ArrayListImitationTest {

    private static ImitationArrayList list;

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();


    @BeforeClass
    public static void beforeAllTests() {
        System.out.println("The place all ArrayList tests will be testing is below \n");
        list = new ImitationArrayList(10);
    }

    @Before
    public void init() {
        list = new ImitationArrayList(2);
    }

    @Test
    public void shouldResizeArray() {
        list.addElement(8);
        list.addElement(7);
        list.addElement(9);
        Assert.assertEquals("Array size should be : 4", 4, list.getIntArray().length);
    }

    @Test
    public void shouldShowAddedElement() {
        list.addElement(5);
        Assert.assertEquals("First element should be 5", 5, list.getIntArray()[0]);
    }

    @Test
    public void shouldShowIncreasedArrayCapacity() {
        list.increaseCapacity(5);
        Assert.assertEquals("Array capacity should be 7", 7, list.getIntArray().length);
    }

    @Test
    public void shouldShowRedusedArrayCapacity() {
        list.reduceCapacity(1);
        Assert.assertEquals("Array size sould be 1", 1, list.getIntArray().length);
    }

    @Test
    public void shouldShowPrintedArray() {
        list.addElement(3);
        list.addElement(5);
        list.printArr();
        Assert.assertEquals("Should show : 35", "35", systemOutRule.getLog());
        list.addElement(4);
        list.addElement(5);
        list.printArr();
        Assert.assertEquals("Should show : 3545", "353545", systemOutRule.getLog());
    }

    @Test
    public void shouldShowSortedArray() {
        list = new ImitationArrayList(3);
        list.addElement(6);
        list.addElement(-1);
        list.addElement(4);
        list.sortArray();
        int[] intArray = list.getIntArray();
        Assert.assertEquals("Should", -1, intArray[0]);
        Assert.assertEquals("d", 4, intArray[1]);
    }

    @Test
    public void shouldShowPrintedInverseArray() {
        list.addElement(43);
        list.addElement(3);
        list.addElement(74);
        list.addElement(54);
        list.printArrInverse();
        Assert.assertEquals("Should return : 5474343", "54 74 3 43 ", systemOutRule.getLog());
    }

    @Test
    public void showWritedLog() {
        list.addElement(777);
        List<String> logList = new ArrayList<>();
        try {
            logList = Files.readAllLines(Paths.get("./log/log.txt"), Charset.defaultCharset());
        } catch (IOException x) {
            x.printStackTrace();
        }
//        Assert.assertEquals(list.date + " addElement -> " + 777, logList.get(logList.size() - 1));
    }

    @Test
    public void showAllLogFile() {
        List<String> loglist = null;
        Path path = Paths.get("./log/log.txt");
        try {
            loglist = Files.readAllLines(path, Charset.defaultCharset());
        } catch (IOException x) {
            x.printStackTrace();
        }

        StringBuilder sb = new StringBuilder();
        for (String s : loglist) {
            sb.append(s + "\n");
        }

//        loglist.forEach(search_for_pair_numbers -> sb.append(search_for_pair_numbers));


        list.addElement(12);

        Assert.assertEquals(true, sb.toString().contains("addElement -> 12"));
    }

}
