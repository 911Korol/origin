package com.lessons.learn_class;

/**
 * Created by 007 on 29.04.2017.
 */
public class LearnClass {

    public static final int COUNT;
    public String name;
    private int count;

    {
//        System.out.println("Non-static block initialization");
    }

    static {
        COUNT = 45;
//        System.out.println("Static block initialization");
    }

    public LearnClass(final int newCount) {
        this.count = newCount;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
