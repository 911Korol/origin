package com.lessons.for_testing;

/**
 * Created by 007 on 28.05.2017.
 */
public class Door {

    public int getNumberOfDoor() {
        testDoNothing();
        return 4;
    }

    public void shouldThrowException() {
        throw new NullPointerException();
    }

    public void testDoNothing() {
        System.out.println("Do nothing");
    }

    public static void main(String[] args) {
        Role comicRole = new Role();
        User user = new User();
        user.setName("Alesha");
        user.setPassword("123");
        comicRole.setComic("Comic");
        user.setRole(comicRole);//копия comicRole
    }

}

class Role {
    private String tragic;
    private String comic;

    public void setTragic(String tragic) {
        this.tragic = tragic;
    }


    public void setComic(String comic) {
        this.comic = comic;
    }
}

class User {
    private String password;
    private String name;
    private Role role;

    public User() {
        role = new Role();
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Role getRole() {

        return role;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }
}

