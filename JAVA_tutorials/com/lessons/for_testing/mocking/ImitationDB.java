package com.lessons.for_testing.mocking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by 007 on 19.05.2017.
 */
public class ImitationDB {
    private List<String> data;

    public ImitationDB() {
        this.data = new ArrayList<>();
        data.addAll(Arrays.asList("Object1", "Object2", "Object3"));
    }

    public List<String> getAllRecords() {
        return data;
    }

    public void insertData(String object) {
        data.add(object);
    }

    public void delete(int index) {
        data.remove(index);
    }

}
