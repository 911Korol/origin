package com.lessons.for_testing.mocking;

import java.util.List;

/**
 * Created by 007 on 19.05.2017.
 */
public class DAOTest {
    private ImitationDB db;

    public DAOTest(ImitationDB db) {
        this.db = db;
    }

    public void insert(String object) {
        db.insertData(object);
    }

    public void delete(int index) {
        db.delete(index);
    }

    public List<String> getAll() {
        return db.getAllRecords();
    }
}
