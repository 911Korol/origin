package com.lessons.for_testing;

/**
 * Created by 007 on 11.05.2017.
 */
public class Calculator {

    public int multiply(int valueOne, int valueTwo) {
        return valueOne * valueTwo;
    }

    public int minus(int valueOne, int valueTwo) {
        return valueOne - valueTwo;
    }


    public void thrownException() {
        throw new NullPointerException("NPE");
    }

    public void print() {
        System.out.print("Hello, ArrayList");
    }
}
