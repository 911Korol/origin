package com.lessons.cloning;

public class TestCloning {
    public static void main(String[] args) throws CloneNotSupportedException {
        Address address = new Address("Kiev");
        CloningStudy original = new CloningStudy(12, "test");
        original.setAddress(address);
        CloningStudy clone = original.clone();
//        clone.setCount(23);
//        clone.setName("NewName");
        clone.getAddress().setCity("Dnepr");

//        System.out.println(original.getCount());
//        System.out.println(original.getName());
        System.out.println(original.getAddress().getCity());
    }
}
