package com.lessons.cloning;

public class CloningStudy implements Cloneable {
    private int count;
    private String name;
    private Address address;

    public CloningStudy(int count, String name) {
        this.count = count;
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected CloningStudy clone() throws CloneNotSupportedException {
        CloningStudy cloningStudy = (CloningStudy) super.clone();
        Address address = cloningStudy.getAddress().clone();
        cloningStudy.setAddress(address);
        return cloningStudy;
    }
}
