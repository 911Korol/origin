package com.company.Lesson18;

import java.util.HashSet;
import java.util.Set;

/* Множество всех животных
1. Внутри класса Solution создать public static классы Cat, Dog.
2. Реализовать метод createCats, котороый должен возвращать множество с 4 котами.
3. Реализовать метод createDogs, котороый должен возвращать множество с 3 собаками.
4. Реализовать метод join, котороый должен возвращать объединенное множество всех животных - всех котов и собак.
5. Реализовать метод removeCats, котороый должен удалять из множества pets всех котов, которые есть в множестве cats.
6. Реализовать метод printPets, котороый должен выводить на экран всех животных, которые в нем есть. Каждое животное с новой строки
*/
public class Part02 {
    public static void main(String[] args) {
        Set<Cat> catSet = catSet();
        Set<Dog> dogSet = dogtSet();
        Set<Object> all = join(catSet, dogSet);
        Set<Set> removeAllCat = removeAllCat(all, catSet);
        printPets(all);

    }


    public static class Cat {
        String name;

        public Cat(String name) {
            this.name = name;
        }
    }

    public static class Dog {
        String name;

        public Dog(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Dog{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

    public static Set catSet() {
        Set<Cat> catSet = new HashSet<>();
        catSet.add(new Cat("Vaska"));
        catSet.add(new Cat("Aska"));
        catSet.add(new Cat("Vaka"));
        catSet.add(new Cat("Aka"));
        return catSet;
    }

    public static Set dogtSet() {
        Set<Dog> dogSet = new HashSet<>();
        dogSet.add(new Dog("Vaska"));
        dogSet.add(new Dog("Aska"));
        dogSet.add(new Dog("Vaka"));
        return dogSet;
    }

    public static Set join(Set<Cat> cat, Set<Dog> dog) {
        Set<Object> all = new HashSet<>();
        all.addAll(dog);
        all.addAll(cat);
        return all;
    }

    public static Set removeAllCat(Set<Object> all, Set<Cat> cats) {
        all.removeAll(cats);
        return all;
    }

    public static Set removeAllDog(Set<Object> all, Set<Dog> dogs) {
        all.removeAll(dogs);
        return all;
    }

    public static void printPets(Set<Object> pets) {
        for (Object pet : pets) {
            System.out.println(pet);

        }

    }
}
