package com.company.Lesson18;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


/* Set из котов
1. Внутри класса создать public static класс кот – Cat.
2. Реализовать метод createCats, он должен создавать множество (Set) котов и добавлять в него 3 кота.
3. В методе main удалите одного кота из Set cats.
4. Реализовать метод printCats, он должен вывести на экран всех котов, которые остались во множестве. Каждый кот с новой строки.
*/
public class Part01 {
    public static void main(String[] args) {
        Set<Cat>  newCat = createCat();
        Iterator<Cat> iterator = newCat.iterator();
        while (iterator.hasNext()){
            iterator.next();
            if(newCat.size() == 3){
                iterator.remove();
            }
        }
        for (Cat cat : newCat) {
            System.out.println(cat);
        }

    }

    public static class Cat{
        String name;

        public Cat(String name){
            this.name = name;
        }
    }

    public static Set createCat (){
        Set<Cat> catNew = new HashSet<>();
        catNew.add(new Cat("Vaska"));
        catNew.add(new Cat("Vaka"));
        catNew.add(new Cat("Aska"));
        return catNew;
    }
}
