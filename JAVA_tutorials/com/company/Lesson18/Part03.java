package com.company.Lesson18;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* Минимальное из N чисел
1. Ввести с клавиатуры число N.
2. Считать N целых чисел и заполнить ими список - метод getIntegerList.
3. Найти минимальное число среди элементов списка - метод getMinimum.
*/
public class Part03 {
    public static void main(String[] args) throws IOException {
List<Integer> list = getInteger();
getMin(list);
    }
    public static List getInteger() throws IOException {
        BufferedReader reader = new BufferedReader (new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            list.add(Integer.parseInt(reader.readLine()));
        }
        return list;

        }
        public static void getMin(List<Integer> list){
            int a = list.get(0);
            for (int i = 1; i < list.size(); i++) {
                if (a > list.get(i)){
                    a = list.get(i);
                }
            } System.out.println(a);

        }

    }

