package com.company.Lesson25_OOP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* Задача по алгоритмам
        Задача: Пользователь вводит с клавиатуры список слов (и чисел). Слова вывести в возрастающем порядке, числа - в убывающем.
        Пример ввода:
        Вишня
        1
        Боб
        3
        Яблоко
        2
        0
        Арбуз
        Пример вывода:
        Арбуз
        3
        Боб
        2
        Вишня
        1
        0
        Яблоко
        */
public class Part01 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<String> list = new ArrayList<>();
        while (true){
            String s = reader.readLine();
            if (s.isEmpty()){break;
            }else {
                list.add(s);
            }
        }
        String[] array = list.toArray(new String[(list.size())]);
        sort(array);

        for (String s : array) {
            System.out.println(s);
        }
    }

    public static boolean is_Number(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException a) {
            return false;
        }
    }

    public static String[] sort(String[] array) throws IOException { // fregre 5 grg 7
        for (int i = 0; i < array.length; i++) {
            if (is_Number(array[i])) {
                for (int j = 0; j < array.length - 1; j++) {
                    if (is_Number(array[j])) {
                        int a = Integer.parseInt(array[i]);
                        int b = Integer.parseInt(array[j]);
                        if (a > b) {
                            String x = array[i];
                            array[i] = array[j];
                            array[j] = x;
                        }
                    }
                }
            } else {
                for (int j = 0; j < array.length - 1; j++) {
                    if (!is_Number(array[j])) {
                        if (isBigger(array[j], array[i])) {
                            String x = array[j];
                            array[j] = array[i];
                            array[i] = x;
                        }
                    }
                }
            }
        }
        return array;
    }

    public static boolean isBigger(String a, String b){
        return a.compareTo(b)>0;
    }

}
