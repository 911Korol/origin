package com.company.Lesson25_OOP;

import java.io.*;
import java.util.List;

/*
Задача: Программа вводит два имени файла. И копирует первый файл на место заданное вторым именем.
*/
public class Part02 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = reader.readLine();
        String fileName2 = reader.readLine();

        InputStream inputStream = new FileInputStream(fileName1);//new FileInputStream(fileName1) является наследником
        // абстрактного класса
        OutputStream outputStream = new FileOutputStream(fileName2);

        int count = 0;

        while(inputStream.available() > 0){//почему available возвращает -1
            int date = inputStream.read();//Можно ли без инт дейт, а сразу в параметры записывать инпут
            outputStream.write(date);
            count++;
        }

        System.out.println("Copies byte: " + count);

        inputStream.close();
        outputStream.close();
    }
}
