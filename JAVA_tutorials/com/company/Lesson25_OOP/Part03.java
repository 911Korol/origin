package com.company.Lesson25_OOP;

import java.io.*;

/*
https://github.com/Korol911/Korol.git
Новая задача: Программа вводит два имени файла. И копирует первый файл на место, заданное вторым именем.
Если файла (который нужно копировать) с указанным именем не существует, то
программа должна вывести надпись «Файл не существует.» и еще раз прочитать имя файла с консоли, а уже потом считывать файл для записи.
*/
public class Part03 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        String file2 = reader.readLine();

        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file1);
        } catch (FileNotFoundException a) {
            System.out.println("Файл не существует");
            String seсondOne = reader.readLine();
            inputStream = new FileInputStream(seсondOne);
        }

        OutputStream outputStream = new FileOutputStream(file2);

        int count = 0;
        while (inputStream.available() > 0) {
            int date = inputStream.read();
            outputStream.write(date);
            count++;
        }
        System.out.println("Скопировано быйт : " + count);

        inputStream.close();
        outputStream.close();
    }
}
