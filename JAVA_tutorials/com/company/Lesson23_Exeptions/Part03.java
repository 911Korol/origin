package com.company.Lesson23_Exeptions;

/* Исключение при работе со строками
Перехватить исключение (и вывести его на экран), указав его тип, возникающее при выполнении кода:
String search_for_pair_numbers = null;
String m = search_for_pair_numbers.toLowerCase();
*/
public class Part03 {
    public static void main(String[] args) {
        try {
            String s = null;
            String m = s.toLowerCase();
        }catch (NullPointerException ex){
            System.out.println("can not");
        }
    }
}
