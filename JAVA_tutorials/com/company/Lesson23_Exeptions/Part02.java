package com.company.Lesson23_Exeptions;

/* Исключение при работе с числами
Перехватить исключение (и вывести его на экран), указав его тип, возникающее при выполнении кода:
int a = 42 / 0;
*/
public class Part02 {
    public static void main(String[] args) {
       try{ int a = 42 / 0;
        System.out.println(a);
       }catch (ArithmeticException a){
           System.out.println("-");
       }

    }
}
