package com.company.Lesson23_Exeptions;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;


public class Part06 {
    public static void main(String[] args) throws URISyntaxException {

        try {
            exept();
        }catch (NullPointerException ex){
            System.out.println("pointer");
        }catch (FileNotFoundException ex){
            System.out.println("file not found");
        }


    }

    public static void exept() throws FileNotFoundException, URISyntaxException {
        int x = (int) (Math.random() * 4);
        if (x == 0) {
            throw new NullPointerException();
        }
        if (x == 1) {
            throw new ArithmeticException();
        }
        if (x == 2) {
            throw new FileNotFoundException();
        }
        if (x == 3) {
            throw new URISyntaxException("", "");
        }
    }
}