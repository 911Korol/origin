package com.company.Lesson23_Exeptions;

import java.util.ArrayList;
import java.util.List;

/* Исключение при работе с коллекциями List
Перехватить исключение (и вывести его на экран), указав его тип, возникающее при выполнении кода:

*/
public class Part05 {
    public static void main(String[] args )  {
        List<String> list = new ArrayList<>();
        try{String s = list.get(18);}
        catch (IndexOutOfBoundsException a){
            System.out.println("______________");
        }

    }

}
