package com.company.Lesson23_Exeptions;

/**
 * Created by Михаил on 18.02.2017.
 *
 * throws
 * try
 * catch
 * finally
 * throw (создавть, выбросить ошибку)
 *
 * checked
 * unchecked
 *
 * Throwable
 * - Exception - checked/unchecked
 *  - IOException - checked ()
 *  - RuntimeException - unchecked (от него наследуются все непроверяемые исключения) (ArrayIndexOutOfBoundsException,
 *  ArithmeticException, NullPointerException, IndexOutOfBoundsException)
 *  - ...
 * - Error - unchecked ()
 */
public class Part01 {
    public static void main(String[] args) {
        int[] arr = new int[2];
        try {
            for (int i = 0; i < arr.length; i++) {
                arr[i] = i;
            }
            System.out.println("TEXT");
           // System.exit(0);
        } catch (ArrayIndexOutOfBoundsException e){
            for (int i : arr) {
                System.out.println(i);
            }
        } catch (Exception e){
            System.out.println("Some exception");
        } finally {
            System.out.println("FINAL");
        }



        System.out.println("text");
    }
}
