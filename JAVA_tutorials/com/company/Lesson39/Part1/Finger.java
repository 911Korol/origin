package com.company.Lesson39.Part1;

/**
 * Created by 007 on 16.04.2017.
 */
public class Finger extends BodyPart {
    private boolean isFoot;

    Finger(String name, boolean isFoot) {
        super(name);
        this.isFoot = isFoot;
    }

    @Override
    public Object containsBones() {
        if (super.containsBones().equals("Yes") && !isFoot) {
            return "Yes";
        } else {
            return "No";
        }
    }


}


