package com.company.Lesson39.Part1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 007 on 16.04.2017.
 */
public class BodyPart implements Alive {
    private String name;

    BodyPart(String name) {
        this.name = name;
    }

    @Override
    public Object containsBones() {
        return "Yes";
    }

    @Override
    public String toString() {
        if (containsBones().equals("Yes")) {
            return name + " содержит кости";
        } else {
            return name + " не содержит кости";
        }
    }

    public static void printlnBodyParts() {
        final BodyPart HAND = new BodyPart("Рука");
        final BodyPart LEG = new BodyPart("Нога");
        final BodyPart HEAD = new BodyPart("Голова");
        final BodyPart BODY = new BodyPart("Тело");
        List<Object> bodypart = new ArrayList<>();
        bodypart.add(HAND);
        bodypart.add(LEG);
        bodypart.add(HEAD);
        bodypart.add(BODY);
        for (Object o : bodypart) {
            System.out.println(o);
        }
    }

}
