package com.company.Lesson14;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* HashMap из 4 пар
Создать коллекцию HashMap<String, String>, занести туда 4 пары строк:
арбуз - ягода, банан - трава, вишня - ягода, груша - фрукт
Вывести содержимое коллекции на экран, каждый элемент с новой строки.
Пример вывода (тут показана только одна строка):
груша - фрукт
*/
public class Part06 {
    public static void main(String[] args) {
        Map<String,String> map = new HashMap<>();
        map.put("Арбуз", " - ягода");
        map.put("Банан", " - трава");
        map.put("Груша", " - фрукт");
        Iterator<Map.Entry<String,String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String,String> entry = iterator.next();
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

    }
}
