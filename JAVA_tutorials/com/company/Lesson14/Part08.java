package com.company.Lesson14;

import java.util.*;

/* Коллекция HashMap из котов
Есть класс Cat с полем имя (name, String).
Создать коллекцию HashMap<String, Cat>.
Добавить в коллекцию 10 котов, в качестве ключа использовать имя кота из массива:
 String[] cats = new String[] {"васька", "мурка", "дымка", "рыжик", "серый", "снежок", "босс", "борис", "визя", "гарфи"};
 В качестве значения использовать экземпляр кота, с переданным именем из массива в конструктор.
 В классе Cat метод toString() должен переводить переданное в конструктор имя в верхний регистр ( метод toUpperCase() )
Вывести результат на экран, каждый элемент с новой строки.
Вывести результат на экран в виде:
васька - ВАСЬКА
мурка - МУРКА
...
*/
public class Part08 {
    public static void main(String[] args) {
        String[] cats = new String[]{"васька", "мурка", "дымка", "рыжик", "серый", "снежок", "босс", "борис", "визя", "гарфи"};//Зачем new String?

//        List<Cat> list = new ArrayList<>();
//        for (int i = 0; i < cats.length; i++) {
//            String name = cats[i];
//            Cat cat1 = new Cat(name);
//            list.add(cat1);
//        }
        Map<String, Cat> col = new HashMap<>();
        for (int i = 0; i < cats.length; i++) {
            col.put(cats[i], new Cat(cats[i]));
        }

//        Iterator<Map.Entry<String, Cat>> iterator = col.entrySet().iterator();
//        while (iterator.hasNext()) {
//            Map.Entry<String, Cat> entry = iterator.next();
//            System.out.println(entry.getKey() + " " + entry.getValue());
//        }

        for (Map.Entry<String, Cat> entry : col.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }

    public static class Cat {
        String name;

        public Cat(String name) {

            this.name = name;
        }

        @Override
        public String toString() {
            return
                   " - " + name.toUpperCase();
        }



    }
}
