package com.company.Lesson14;

import java.util.*;

/**
 * Created by misha on 19.01.17.
 */
public class Part05_Iterator {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("fefe");
        list.add("fefefwefw");
        list.add("fefefw");

        Iterator<String> iterator = list.iterator();
        while(iterator.hasNext()){
            String s = iterator.next();
            iterator.remove();
            System.out.println(s);
        }
        for (String s : list) {
            System.out.println(s);
        }

        System.out.println("---------");
        Set<String> set = new HashSet<>();
        set.add("fefe");
        set.add("fefefwefw");
        set.add("fefefw");

        Iterator<String> iterator1 = set.iterator();
        while(iterator1.hasNext()){
            String s = iterator1.next();
            iterator1.remove();
            System.out.println(s);
        }
        for (String s : set) {
            System.out.println(s);
        }

        System.out.println("---------");
        Map<String, String> map = new HashMap<>();
        map.put("text1", "1");
        map.put("text2", "2");
        map.put("text3", "3");
        Iterator<Map.Entry<String, String>> iterator2 = map.entrySet().iterator();
        while (iterator2.hasNext()){
            Map.Entry<String, String> entry = iterator2.next();
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }
    }
}
