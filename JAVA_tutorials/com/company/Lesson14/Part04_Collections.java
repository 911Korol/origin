package com.company.Lesson14;

import java.util.*;

/**
 * Created by misha on 19.01.17.
 * List - ArrayList, LinkedList, Vector
 * Set - HashSet
 * Map - HashMap, TreeMap
 */

public class Part04_Collections {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("greg");
        list.add("greg");
        list.add("greg334");
        for (String s : list) {
            System.out.println(s);
        }


        Set<String> set = new LinkedHashSet<>();
        set.add("text");
        set.add("text");
        set.add("text1");
        set.add("text2");
        set.add("text3");

        for (String s : set) {
            System.out.println(s);
        }

        Map<String, Integer> map = new HashMap<>();
        map.put("text1", 25);
        map.put("text1", 26);
        map.put("text2", 25);
        map.put("text3", 56);

        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }
    }

}