package com.company.Lesson14;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by misha on 19.01.17.
 */
public class Part07 {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("Tree");
        set.add("Fish");
        set.add("Apple");
        Iterator<String> iterator2 = set.iterator();
        while (iterator2.hasNext()){
            String s = iterator2.next();
            System.out.println(s);
        }
    }
}
