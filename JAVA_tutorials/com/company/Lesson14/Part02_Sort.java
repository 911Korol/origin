package com.company.Lesson14;

/**
 * Created by misha on 19.01.17.
 */
public class Part02_Sort {
    public static void main(String[] args) {
        int[] arr = {5,86,4,67,89};                        //5,86,4,67,89 //5,86,4,67,89    //5,4,86,67,89     //5,4,67,86,89
        for (int i = 0; i < arr.length - 1; i++) {         //i=0; i<4     //i=0; i<4        //i=0; i<4         //i=0; i<4
            for (int j = 0; j < arr.length - 1 -i; j++) {  //j=0; j<4     //j=1; j<4        //j=2; j<4         //j=3; j<4
                if (arr[j] > arr[j +1]){                   //5>86f        //86>4t           //86>67t           //86>89f
                    int a = arr[j + 1];                    //             //a=[2](4)        //a=[3](67)        //
                    arr[j+1] = arr[j];                     //             //[2](4)=[1](86)  //[3](67)=[2](86)  //
                    arr[j] = a;                            //             //[1](86)=a[2](4) //[2](86)=a[3](67) //
                }
            }
        }

                                                            //5,4,67,86,89  //4,5,67,86,89    //4,5,67,86,89     //4,5,67,86, 89
                                                            //i=1; i<4      //i=1; i<4        //i=1; i<4         //i=1; i<4
                                                            //j=0; j<3      //j=1; j<3        //j=2; j<3         //j=3; j<3
                                                            //5>4t          //5>67f           //67>86f           //86>89f
                                                            //[1](4)=[0](5) //                //                 //
                                                            //[0](5)=a      //                //                 //



        for (int i : arr) {
            System.out.println(i);

        }
    }
}
