package com.company.Lesson14;

/**
 * Created by misha on 19.01.17.
 */
public class Part03 {
    public static void main(String[] args) {
        int[] arr = {123, 565, 7, 98, 57, 58, 954, 4543};
        sort(arr);
        for (int i : arr) {
            System.out.println(i);
        }

    }

    public static void sort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    int a = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = a;
                }
            }
        }
    }
}
