package com.company.Lesson11_while_true;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Михаил on 07.01.2017.
 */
public class Part06 {
    public static void main(String[] args) {
        List<Integer> list1 = new ArrayList<>();
        Collections.addAll(list1, 54, 32, 57, 345);
        List<Integer> list2 = new ArrayList<>();
        Collections.addAll(list2, 54, 32, 57, 345);

        List<Integer> list3 = new ArrayList<>();
        list3.addAll(list1);
        list3.addAll(list2);

        for (Integer integer : list3) {
            System.out.println(integer);
        }
    }
}
