package com.company.Lesson11_while_true;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Михаил on 07.01.2017.
 */
public class Part02 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<String> list = new ArrayList<>();
        while (true) {
            String s = reader.readLine();
            if (s.isEmpty()) break;
            list.add(s);
        }

        for (String s : list) {
            System.out.println(s);
        }

        System.out.println("______________");

        List<Integer> list1 = new ArrayList<>();
        while (true) {
            String s = reader.readLine();
            if (s.isEmpty()) break;
            list1.add(Integer.parseInt(s));
        }

        for (Integer integer : list1) {
            System.out.println(integer);
        }
    }

}
