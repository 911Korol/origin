package com.company.Lesson11_while_true;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* чётные числа добавляются в конец списка, нечётные – в начало
 */
public class Part03 {
    public static void main(String[] args) throws IOException {
//        int number = 7;
//        if(number % 2 == 0)
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<Integer> list = new ArrayList<>();
        while (true){
            String s = reader.readLine();
            if(s.isEmpty()){
                break;
            }
            int x = Integer.parseInt(s);
            if (x % 2 == 0){
                list.add(x);
            }else {
                list.add(0,x);
            }
        }
        System.out.println(list);
    }
}
