package com.company.Lesson11_while_true;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Удаление всех чисел больше 5
 */
public class Part04 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<Integer> list = new ArrayList<>();

        while (true) {
            String s = reader.readLine();
            if (s.isEmpty()) {
                break;
            }
            int x = Integer.parseInt(s);
            list.add(x);

        }

        for (int i = 0; i < list.size(); ) {
            if (list.get(i) > 5) {
                list.remove(i);
            } else {
                i++;
            }

        }
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));

        }
    }
}
