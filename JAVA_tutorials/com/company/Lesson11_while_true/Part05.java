package com.company.Lesson11_while_true;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/*Pазделение массива на два – чётных и нечётных чисел
 */
public class Part05 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<Integer> list = new ArrayList<>();
        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();
        while (true){
            String s = reader.readLine();
            if (s.isEmpty())
                break;
            list.add(Integer.parseInt(s));
        }
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) % 2 == 0){
                list2.add(list.get(i));
            }else {
                list1.add(list.get(i));
            }

        }
        for (int i = 0; i < list2.size(); i++) {
            System.out.println(list2.get(i));

        }


    }
}
