package com.company.Lesson38;

/**
 * Created by 007 on 09.04.2017.
 * [](List) [8](List -> el2) [12](List = el1, el3, el7) [](List) [] ... // 16  O(1) O(n)
 */
public class Lesson01 {
    int a;
    String b;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Lesson01 lesson01 = (Lesson01) o;

        if (a != lesson01.a) return false;
        return b != null ? b.equals(lesson01.b) : lesson01.b == null;
    }

    @Override
    public int hashCode() {
        int result = a;
        result = 31 * result + (b != null ? b.hashCode() : 0);
        return result;
    }
}
