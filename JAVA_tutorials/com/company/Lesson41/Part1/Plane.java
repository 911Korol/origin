package com.company.Lesson41.Part1;

/**
 * Created by 007 on 27.04.2017.
 */
public class Plane implements Flyable {
    private int a;
    public Plane(int a){
        this.a = a;
    }

    @Override
    public String fly() {
        return "Plane is flying";
    }
}
