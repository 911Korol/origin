package com.company.Lesson41;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Статики
 * 1. В статическом блоке считайте две переменные с консоли number1 и number2 с типом int.
 * 2. Не забыть про IOException, который надо обработать в блоке catch.
 * 3. Закрыть поток ввода методом close().
 * 4. Создать метод min(int a, int b), который должен вернуть минимальное значение между number1 и number2.
 * 5. Создать статическую, неизменяемую переменную MIN и присвоить ей значение, которое возвращает метод min.
 * 6. В выполняющем методе вывести значение переменной MIN.
 */
public class Part01 {
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    static int number1;
    static int number2;

    static {
        try {
            number1 = Integer.parseInt(reader.readLine());
            number2 = Integer.parseInt(reader.readLine());
            reader.close();
        } catch (IOException x) {
        }
    }

    static final int MIN = min(number1, number2);

    public static int min(int number1, int number2) {
        int x;
        if (number1 < number2) {
            x = number1;
        } else {
            x = number2;
        }
        return x;
    }

    public static void main(String[] args) {
        System.out.println(MIN);
    }
}
