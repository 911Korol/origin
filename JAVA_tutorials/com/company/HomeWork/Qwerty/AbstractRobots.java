package com.company.HomeWork.Qwerty;


/**
 * Created by Михаил on 27.03.2017.
 */
public abstract class AbstractRobots implements Atacking, Defending {
    private String name;
    private int health;

    public AbstractRobots(String name, int health) {
        this.name = name;
        this.health = health;
    }

    public String getName() {
        return name;
    }

    public int getHealth() {
        return health;
    }

    private static int hit;

    @Override
    public BodyParts atack() {
        BodyParts atack = null;
        hit = (int) (Math.random() * 4);
        if (hit == 0) {
            atack = BodyParts.Hand;
        }
        if (hit == 1) {
            atack = BodyParts.Leg;
        }
        if (hit == 2) {
            atack = BodyParts.Head;
        }
        if (hit == 3) {
            atack = BodyParts.Chest;
        }

        return atack;
    }

    @Override
    public BodyParts defend() {
        BodyParts defend = null;
        hit = (int) (Math.random() * 4);
        if (hit == 0) {
            defend = BodyParts.Hand;
        }
        if (hit == 1) {
            defend = BodyParts.Leg;
        }
        if (hit == 2) {
            defend = BodyParts.Head;
        }
        if (hit == 3) {
            defend = BodyParts.Chest;
        }

        return defend;
    }


}

