package com.company.HomeWork.Qwerty;

/**
 * Created by Михаил on 27.03.2017.
 */
public class BodyParts {
    String bodyparts;
    public BodyParts(String bodyparts){
        this.bodyparts = bodyparts;
    }

    @Override
    public String toString() {
        return  bodyparts ;
    }

    final static BodyParts Hand = new BodyParts("Hand");
    final static BodyParts Leg = new BodyParts("Leg");
    final static BodyParts Head = new BodyParts("Head");
    final static BodyParts Chest = new BodyParts("Chest");
}
