package com.company.HomeWork.NewRobot;

/**
 * Created by Михаил on 29.03.2017.
 */
public class BodyPart {
    String bodypart;

    public BodyPart(String bodypart) {
        this.bodypart = bodypart;
    }

    @Override
    public String toString() {
        return bodypart;
    }

    final static BodyPart Hand = new BodyPart("Hand");
    final static BodyPart Head = new BodyPart("Head");
    final static BodyPart Leg = new BodyPart("Leg");
}
