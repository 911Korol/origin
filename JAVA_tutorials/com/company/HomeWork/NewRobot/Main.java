package com.company.HomeWork.NewRobot;

/**
 * Created by Михаил on 29.03.2017.
 */
public class Main {
    public static void main(String[] args) {
        AbstractFabric first = new Robot("Rex", 100);
        AbstractFabric second = new Robot("Cord", 100);
        doMove(first, second);
    }

    public static void doMove(AbstractFabric x, AbstractFabric y) {
        BodyPart atacking = x.atack();
        BodyPart defending = y.defend();
        if (atacking == defending) {
            System.out.println(String.format("Robot %search_for_pair_numbers attacked of robot %search_for_pair_numbers, %search_for_pair_numbers had been attacked, %search_for_pair_numbers has been protected ", x.getName(), y.getName(), x.atack(), y.defend()));
        }
    }
}
