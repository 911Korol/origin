package com.company.HomeWork.NewRobot;

/**
 * Created by Михаил on 29.03.2017.
 */
public abstract class AbstractFabric implements Attacking,Defending {
    private String name;
    private int health;
    public AbstractFabric(String name, int health){
        this.name = name;
        this.health = health;
    }
    public String getName(){return name;}
    public int getHealth(){return health;}
    @Override
    public BodyPart defend() {
        BodyPart def = null;
        int x = (int)(Math.random()*3);
        if (x==0){
            def=BodyPart.Hand;}
            if (x==1){
            def = BodyPart.Head;
            }if (x==2){
                def=BodyPart.Leg;
        }
        return def;
    }

    @Override
    public BodyPart atack() {

        BodyPart atack = null;
        int y = (int) (Math.random() * 3);
        if (y==0){
            atack=BodyPart.Hand;}
        if (y==1){
            atack= BodyPart.Head;
        }if (y==2){
            atack=BodyPart.Leg;
        }
        return atack;
    }
}