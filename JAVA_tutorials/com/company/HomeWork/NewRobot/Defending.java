package com.company.HomeWork.NewRobot;

/**
 * Created by Михаил on 29.03.2017.
 */
public interface Defending {
    BodyPart defend();
}
