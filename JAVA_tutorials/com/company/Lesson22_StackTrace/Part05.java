package com.company.Lesson22_StackTrace;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Михаил on 16.02.2017.
 */
public class Part05 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<Integer> list = new ArrayList<>();
        while (true) {
            String s = reader.readLine();
            if (s.isEmpty()) {
                break;
            } else {
                int r = Integer.parseInt(s);
                list.add(r);
            }
        }
        Integer[] array = list.toArray(new Integer[(list.size())]);

        arr(array);
    }

    public static Integer[] arr(Integer[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    int a = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = a;
                }

                }
            } for (Integer integer : array) {
                System.out.println(integer);


            }
        return array;
    }
}
