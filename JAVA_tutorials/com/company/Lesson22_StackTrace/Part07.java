package com.company.Lesson22_StackTrace;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Михаил on 16.02.2017.
 */
public class Part07 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<String> list = new ArrayList<>();
        while (true) {
            String s = reader.readLine();
            if (s.isEmpty()) {
                break;
            } else {
                list.add(s);
            }
        }
        String[] array = list.toArray(new String[(list.size())]);

        for (int i = 0; i < array.length-1; i++) {
            for (int j = 0; j < array.length-1-i; j++) {
                if (isBigger(array[j],array[j+1])){
                    String s = array[j+1];
                    array[j+1]=array[j];
                    array[j]= s;
                }
            }

        }
        for (String s : array) {
            System.out.println(s);
        }
    }
    public static boolean isBigger(String a, String b){
        return a.compareTo(b)>0;
    }
}
