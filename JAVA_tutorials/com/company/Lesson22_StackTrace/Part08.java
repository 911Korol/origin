package com.company.Lesson22_StackTrace;

import java.util.Date;

/**
 * Created by Михаил on 16.02.2017.
 */
public class Part08 {
    public static void main(String[] args) {
        int a = 0;
        int b = 1;
        Date date1 = new Date();
        for (int i = 0; i < 10; i++) {
            int c = a + b;
            a = b;
            b = c;
            if (c == 1) {
                System.out.println(0);
                System.out.println(c);
            } else {
                if (c < 11) {
                    System.out.println(c);
                }
            }
        }
        Date date2 = new Date();
        System.out.println(date2.getTime()-date1.getTime());
    }
}
//1 2 3 4 5 6
//
//1+2=3
//2+3=5
//3+5=8