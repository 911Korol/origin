package com.company.Lesson22_StackTrace;

/**
 * /* Метод должен вернуть номер строки кода, из которого вызвали этот метод
 * Написать пять методов, которые вызывают друг друга. Метод должен вернуть номер строки кода,
 * из которого вызвали этот метод. Воспользуйся функцией: element.getLineNumber().
 */
public class Part02 {
    public static void main(String[] args) {
        method1();
    }

    public static void method1() {
        method2();
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        int s = elements[2].getLineNumber();
        System.out.println(s);
    }

    public static void method2() {
        method3();
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        int s = elements[2].getLineNumber();
        System.out.println(s);

    }

    public static void method3() {
        method4();
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        int s = elements[2].getLineNumber();
        System.out.println(s);

    }

    public static void method4() {
        method5();
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        int s = elements[2].getLineNumber();
        System.out.println(s);

    }

    public static void method5() {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        int s = elements[2].getLineNumber();
        System.out.println(s);

    }
}
