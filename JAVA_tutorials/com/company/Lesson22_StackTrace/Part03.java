package com.company.Lesson22_StackTrace;

/* Метод должен возвращать результат – глубину его стек-трейса
Написать метод, который возвращает результат – глубину его стек трейса –
количество методов в нем (количество элементов в списке). Это же число метод должен выводить на экран.
*/
public class Part03 {
    public static int main(String[] args) {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        int s = elements.length;
        for (StackTraceElement element : elements) {
            System.out.println(element);
        }
        System.out.println(s);
        return s;
    }


}
