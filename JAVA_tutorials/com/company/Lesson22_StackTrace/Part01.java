package com.company.Lesson22_StackTrace;

/* И снова StackTrace
Написать пять методов, которые вызывают друг друга. Каждый метод должен возвращать имя метода, вызвавшего его, полученное с помощью StackTrace.
*/
public class Part01 {
    public static void main(String[] args) {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        String element = elements[2].getMethodName();
        System.out.println(element);
        metod();

    }
    public static void metod(){

        StackTraceElement [] elements = Thread.currentThread().getStackTrace();
        String element = elements[2].getMethodName();
        System.out.println(element);
        metod1();
    }

    public static void metod1(){

        StackTraceElement [] elements = Thread.currentThread().getStackTrace();
        String element = elements[2].getMethodName();
        System.out.println(element);
        metod2();
    }
    public static void metod2(){

        StackTraceElement [] elements = Thread.currentThread().getStackTrace();
        String element = elements[2].getMethodName();
        System.out.println(element);
        metod3();
    }
    public static void metod3(){
        StackTraceElement [] elements = Thread.currentThread().getStackTrace();
        String element = elements[2].getMethodName();
        System.out.println(element);
    }
}
