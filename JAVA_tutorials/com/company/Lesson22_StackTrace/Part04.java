package com.company.Lesson22_StackTrace;

/* Стек-трейс длиной 10 вызовов
Напиши код, чтобы получить стек-трейс длиной 10 вызовов.
*/
public class Part04 {
    public static void main(String[] args) {
        method1();
    }
    public static void method1(){

        method2();

    }
    public static void method2(){
       method3();

    }
    public static void method3(){
        StackTraceElement[] element = Thread.currentThread().getStackTrace();
        System.out.println(element.length);
    }
}
