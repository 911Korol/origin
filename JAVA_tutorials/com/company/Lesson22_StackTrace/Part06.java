package com.company.Lesson22_StackTrace;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Михаил on 16.02.2017.
 */
public class Part06 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<String> list = new ArrayList<>();
        while (true){
            String s = reader.readLine();
            if (s.isEmpty()){
                break;}else  {
                list.add(s);
            }
        }
        String[] array = list.toArray(new String[(list.size())]);
    arr(array);
        for (String s : array) {
            System.out.println(s);
        }
    }
    public static String[] arr (String[] array) {
        for (int i = 0; i < array.length-1;) {
            if (isBigger(array[i],array [i+1])) {
                String s = array[i + 1];
                array[i + 1] = array[i];
                array[i] = s;
                if (i>0){
                    i--;
                }
            } else {
                i++;
            }
        }return array;
    }
    public static boolean isBigger(String a, String b){
        return a.compareTo(b)>0;
    }
}
