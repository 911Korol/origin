package com.company.Lesson16_what_is_faster;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

/* Удалить все числа больше 10
Создать множество чисел(Set<Integer>), занести туда 10 различных чисел.
При помощи метода  removeAllNumbersMoreThan10 удалить из множества все числа больше 10.
*/
public class Part04 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Set<Integer> any = new HashSet<>();
        for (int i = 0; i < 5; i++) {
            any.add( Integer.parseInt(reader.readLine()));
        }
        any = removing(any);
        for (Integer integer : any) {
            System.out.println(integer);
        }
    }

    public static Set removing(Set<Integer> bigmas){
        Set<Integer> any2 = new HashSet<>();
        for (Integer bigma : bigmas) {
            if (bigma < 10){
                any2.add(bigma);
            }
        }return any2;

    }

}
