package com.company.Lesson16_what_is_faster;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/* Измерить сколько времени занимает 10 тысяч вызовов get для каждого списка
Измерить, сколько времени занимает 10 тысяч вызовов get для каждого списка.
Метод getTimeMsOfGet  должен вернуть время его исполнения в миллисекундах.
*/
public class Part02 {
    public static void main(String[] args) {
        dat(ar(new ArrayList()));
        dat(ar(new LinkedList()));
        dat(getting(new ArrayList()));
        dat(getting(new LinkedList()));
    }

    public static List ar(List list){
        for (int i = 0; i < 100000; i++) {
            list.add(new Object());
        }
        return list;
    }

    public static List getting(List list){
        for (int i = 0; i < list.size(); i++) {
            list.get(i);
        }
        return list;
    }
    public static void dat(List list){
        Date dat1 = new Date();
        getting(list);
        Date dat2 = new Date();
        System.out.println(dat2.getTime() - dat1.getTime());
    }
}
