package com.company.Lesson16_what_is_faster;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/* Измерить сколько времени занимает 10 тысяч вставок для каждого списка
Измерить, сколько времени занимает 10 тысяч вставок для каждого списка.
Метод getTimeMsOfInsert  должен вернуть время его исполнения в миллисекундах.
*/
public class Part01 {
    public static void main(String[] args) {
        getTimeMsOfInsert(new ArrayList());
        getTimeMsOfInsert(new LinkedList());
    }

    public static void insert(List list) {
        for (int i = 0; i < 100_000; i++) {
            list.add(0, new Object());
        }
    }

    public static void getTimeMsOfInsert(List list) {
        Date date1 = new Date();
        insert(list);
        Date date2 = new Date();
        System.out.println(date2.getTime() - date1.getTime());
    }
}
