package com.company.Lesson35.Part01;

/**
 * Created by Михаил on 01.04.2017.
 */
public  class USD extends Money {
    public USD(int x) {
        super(x);
    }

    @Override
    public int getAmount() {
        return super.getAmount();
    }

    String getCurrencyName() {
        return "USD";
    }
}
