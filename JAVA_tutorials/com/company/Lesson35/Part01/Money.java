package com.company.Lesson35.Part01;

/**
 * Created by Михаил on 01.04.2017.
 */
public abstract class Money {
    private int x;
    public int getAmount() {
        return x;
    }

    public Money(int x) {
        this.x = x;
    }
    abstract  String getCurrencyName();

}
