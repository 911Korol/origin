package com.company.Lesson35;

/**
 * Created by Михаил on 01.04.2017.
 */
public class Part1 {
    static int x = 1;

    public Part1(int a) {
        x += 2;
    }
}

class Part02 extends Part1{
    public Part02() {
        super(5);
        x+=1;
    }

//    public static void main(String[] args) {
//        Part02 p = new Part02();
//        System.out.println(x);
//    }
}

 class Testing {
    private int value = 1;

    public int getValue() {
        return value;
    }

    public void changeVal(int value) {
        this.value = value;
    }

    public static void main(String args[]) {
        int a = 2;
        Testing c = new Testing();
        c.changeVal(a);
        System.out.print(c.getValue());
    }
}

