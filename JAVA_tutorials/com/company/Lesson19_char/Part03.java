package com.company.Lesson19_char;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* Пять наибольших чисел
Создать массив на 10 чисел. Заполнить его числами с клавиатуры. Вывести пять наибольших чисел.
*/
public class Part03 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = Integer.parseInt(reader.readLine());
        }
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1 -i; j++) {
                if (array[j] < array[j+1]){
                    int a = array[j+1];
                    array[j+1] = array[j];
                    array[j] = a;
                }
            }

        }
        for (int i = 0; i < array.length - 5; i++) {
            System.out.println(array[i]);
        }
    }
}
