package com.company.Lesson19_char;

/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран
Имя: ded Ivan, пол: мужской, возраст: 70, дети: papa Fedya
Имя: baba Masha, пол: женский, возраст: 65, дети: papa Fedya
Имя: papa Fedya, пол: мужской, возраст: 40, дети: son Lesha, son Misha, daughter Masha*/


import java.util.ArrayList;
import java.util.List;

public class Part02 {
    public static void main(String[] args) {
        List<Human> humans = new ArrayList<>();
        humans.add(new Human("Masha", true, 15, new ArrayList<>()));
        humans.add(new Human("Sasha", false, 13, new ArrayList<>()));
        humans.add(new Human("Dasha", true, 17, new ArrayList<>()));
        List<Human> mom = new ArrayList<>();
        mom.add(new Human("Olga", true, 39, humans));
        List<Human> dad = new ArrayList<>();
        dad.add(new Human("Igor", false, 40, humans));
        Human Dad = new Human("Igor", false, 40, humans);
        Human Mom = new Human("Olga", true, 39, humans);
        Human Grandpa = new Human("Serg", false, 65, dad);
        Human Grandpa2 = new Human("Petr", false, 67, mom);
        Human Grandma = new Human("Nina", false, 65, dad);
        Human Grandma2 = new Human("Masha", false, 65, mom);
        System.out.println(Dad);
        System.out.println(Mom);
        System.out.println(Grandpa);
        System.out.println(Grandpa2);
        System.out.println(Grandma);
        System.out.println(Grandma2);

    }

    public static class Human {
        String name;
        boolean sex;
        int age;
        List<Human> humans = new ArrayList<>();


        public Human(String name, boolean sex, int age, List<Human> humans) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.humans = humans;

        }

        @Override
        public String toString() {
            String text = "";
            text += "Имя " + name;
            if (!sex) {
                text += " Пол : мужской ";
            } else {
                text += " Пол : женский ";
            }
            text += " Возраст: " + age;


            if (humans.size() > 0) {
                text += ", дети: " + humans.get(0).name;
                for (int i = 1; i < humans.size(); i++) {
                    text += ", " + humans.get(i).name;
                }
            }

            return text;
        }
    }
}
