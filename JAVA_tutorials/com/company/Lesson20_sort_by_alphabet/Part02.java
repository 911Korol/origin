package com.company.Lesson20_sort_by_alphabet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/* Номер месяца
Программа вводит с клавиатуры имя месяца и выводит его номер на экран в виде: «May is 5 month».
Используйте коллекции.
*/
public class Part02 {
    public Part02() throws IOException {
    }

    public static void main(String[] args) throws IOException {
        search(date());


    }

    public static Map date() {
        Map<String, Integer> map = new HashMap<>();
        map.put("January", 1);
        map.put("February", 2);
        map.put("March", 3);
        map.put("April", 4);
        map.put("May", 5);
        map.put("June", 6);
        map.put("July", 7);
        map.put("August", 8);
        map.put("September", 9);
        map.put("October", 10);
        map.put("November", 11);
        map.put("December", 12);
        return map;
    }

    public static void search(Map<String, Integer> map) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        for (Map.Entry<String, Integer> integer : map.entrySet()) {
            if (s.equals(integer.getKey())) {
                System.out.println(integer.getKey() + " is " + integer.getValue() + " mounth ");
            }
        }
    }
}

