package com.company.Lesson20_sort_by_alphabet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* Задача по алгоритмам
Задача: Введи с клавиатуры 10 слов и выведи их в алфавитном порядке.
*/
public class Part04 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] array = new String[5];
        for (int i = 0; i < array.length; i++) {
            String s = reader.readLine();
            array[i] = s;
        }

        sort(array);

        for (String s : array) {
            System.out.println(s);
        }
    }

    private static void sort(String[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (isGreaterThen(array[j], array[j + 1])) {
                    String temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    private static boolean isGreaterThen(String a, String b) {
        return a.compareTo(b) > 0;
    }
}
