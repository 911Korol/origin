package com.company.Lesson20_sort_by_alphabet;

/**
 * Created by Михаил on 09.02.2017.
 */
public class Part01Bubble {
    public static void main(String[] args) {
        int[] array = {763,4,34,2,676,3,45,78,53535,78,5};
        for (int i = 0; i <array.length-1 ; i++) {
            for (int j = 0; j <array.length-1-i; j++) {
                if (array[j]>array[j+1]){
                    int a = array[j];
                    array[j]=array[j+1];
                    array[j+1] = a;
                }

            }

        }
        for (int i : array) {
            System.out.println(i);
        }
    }
}
