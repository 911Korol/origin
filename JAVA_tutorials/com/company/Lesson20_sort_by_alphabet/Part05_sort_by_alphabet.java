package com.company.Lesson20_sort_by_alphabet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Михаил on 09.02.2017.
 */
public class Part05_sort_by_alphabet {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] array = new String[5];
        for (int i = 0; i < array.length; i++) {
            String s = reader.readLine();
            array[i] = s;
        }
        comperation(array);
    }

    public static void comperation(String[] array){
        for (int i = 0; i < array.length -1; i++) {
            for (int j = 0; j < array.length-1-i; j++) {
                if (isBiggerThen(array[j],array[j+1])){
                    String s = array[j];
                    array[j] = array[j+1];
                    array[j+1] = s;
                }
            }

            }for (String s : array) {
                System.out.println(s);
        }
    }
    public static boolean isBiggerThen(String a, String b) {
        return a.compareTo(b)>0;
    }
}

