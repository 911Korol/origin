package com.company.Lesson20_sort_by_alphabet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/*
Задача: Программа определяет, какая семья (фамилию) живёт в указанном городе.
Пример ввода:
Москва
Ивановы
Киев
Петровы
Лондон
Абрамовичи

Лондон

Пример вывода:
Абрамовичи
*/
public class Part03 {
    public static void main(String[] args) throws IOException {
living(cities());
    }
    public static Map cities() throws IOException {
        BufferedReader reader = new BufferedReader (new InputStreamReader(System.in));
        Map<String,String> map = new HashMap<>();
        map.put(reader.readLine(), reader.readLine());
        map.put(reader.readLine(), reader.readLine());
        map.put(reader.readLine(), reader.readLine());
        map.put(reader.readLine(), reader.readLine());
        return map;
    }
    public static void living(Map<String,String> map) throws IOException {
        BufferedReader reader = new BufferedReader (new InputStreamReader(System.in));
        String s = reader.readLine();
        for (Map.Entry<String,String> s1 : map.entrySet()) {
            if (s.equals(s1.getKey())){
                System.out.println(s1.getValue());
            }

        }
    }
}
