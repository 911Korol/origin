package com.company.Interview;
/**
 * Created by Михаил on 21.03.2017.
 */
public class ReverseVowelsOfString {
    public static void main(String[] args) {
        String s = "eIaOyU";
        System.out.println(reverseVowelsOfString(s));
    }
    static String wovels = "eEuUiIoOaA";
    public static String reverseVowelsOfString(String s) {
        int i,j;
        char[] string = s.toCharArray();
        for (i = 0, j = string.length - 1; i < j; i++, j--) {
            while (i<string.length && wovels.indexOf(string[i]) == -1) i++;
            while ((j>=0) && wovels.indexOf(string[j]) == -1) j--;
            if (i < j) {
                string[i] ^= string[j];
                string[j] ^= string[i];
                string[i] ^= string[j];
            }
        }
        return new String(string);
    }
}