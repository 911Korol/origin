package com.company.Interview;

/**
 * Created by Михаил on 10.03.2017.
 */
public class ReverseInteger {
    public static void main(String[] args) {
        int x = -4243;
        System.out.println(reverseInteger(x));
    }

    static int reverseInteger(int x) {
        String z = Integer.toString(x);
        StringBuffer newString = new StringBuffer(z);
        if (x < 0) {
            newString.deleteCharAt(0);
            newString.reverse();
            x = Integer.parseInt(newString.toString());
            x*=(-1);
        } else {
            newString.reverse();
            x = Integer.parseInt(newString.toString());
        }
        return x;
    }
}
