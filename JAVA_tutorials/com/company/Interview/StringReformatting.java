package com.company.Interview;


import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class StringReformatting {
    public static void main(String[] args) {
        int k = 4;
        String s = "0908078890708078070807889070807080788907080788907890";
        stringReformatting(s, k);
    }

    public static String stringReformatting(String s, int k) {
        char[] array = s.toCharArray();
        char[] newarray = new char[array.length + array.length / k];
        int j = 0;
        for (int i = array.length - 1; i >= 0; i--) {
            if (array[i] != '-') {
                if (j % (k + 1) == k) {
                    newarray[j] = '-';
                    j++;
                }
                newarray[j] = array[i];
                j++;
            }
        }
        char[] res = new char[j];
        for (int i = 0; i < res.length; i++) {
            res[i] = newarray[j - i - 1];
        }
        for (char re : res) {
            System.out.print(re);
        }
        return new String(res);
    }
}