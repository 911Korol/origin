package com.company.Interview;

/**
 * Created by Михаил on 07.03.2017.
 */
public class ContainsDuplicates {
    public static void main(String[] args) {
        int[] a = {1, 2, 3};
        containsDuplicates(a);
    }

    static boolean containsDuplicates(int[] a) {
        boolean result = true;
        int count = 0;
        for (int i : a) {
            for (int i1 : a) {
                if (i == i1) {
                    count++;
                }
            }
        }
        if (count > a.length) {
            result = true;
        } else if (count < a.length) {
            result = false;
        }

        return result;
    }
}
