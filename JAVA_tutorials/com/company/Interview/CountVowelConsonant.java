package com.company.Interview;

/**
 * Created by Михаил on 14.03.2017.
 */
public class CountVowelConsonant {
    public static void main(String[] args) {
        String s = "abcde";
        System.out.println(countVowelConsonant(s));
    }
    public static int countVowelConsonant(String s) {
        s.toLowerCase();
        int consonant = s.replaceAll("[a,e,u,i,o]", "").length();
        int vovels = s.length()- consonant;
        return consonant*2 + vovels;
    }
}