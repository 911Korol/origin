package com.company.Interview;

import java.util.Arrays;

/**
 * Created by Михаил on 09.03.2017.
 */
public class Test_missingNumber {
    public static void main(String[] args) {
        int[] arr = {0, 1, 2};
        missingNumber(arr);
        System.out.println(missingNumber(arr));

    }

    static int missingNumber(int[] arr) {
        Arrays.sort(arr);
        int x = 0;

        for (int i : arr) {
            if (i != x) {
                break;
            }
            x++;
        }
        return x;
    }
}
