package com.company.Interview;

/**
 * Created by Михаил on 26.02.2017.
 */
public class RemoveDuplicateAdjacent {
    public static void main(String[] args) {

        System.out.println(removeDuplicateAdjacent("mississipie"));
    }

    public static String removeDuplicateAdjacent(String s) {

        int x = s.length();
        int z = 0;
        String a = "";
        while (true) {
            if (x == z) {
                break;
            } else {
                x = s.length();
                s = s.replaceAll("(.)\\1+", "");
                z = s.length();
            }
        }
        return s;
    }
}