package com.company.Interview;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.List;

/**
 * Created by Михаил on 10.03.2017.
 */
public class RemoveAllDuplicate {
    public static void main(String[] args) {
        String s = "cooodefightssforrrrrrrrrrrrrrrcodee";
        System.out.print(removeDuplicateAdjacent(s));
    }

    public static String removeDuplicateAdjacent(String s) {
        char[] chars = s.toCharArray();
        Arrays.sort(chars);
        for (char aChar : chars) {
            System.out.print(aChar);
        }
        System.out.println("");
        String z = "";
        Character l = null;
        for (Character aChar : chars) {
            if (aChar.equals(l)) {
                continue;
            }
            z = z.concat(aChar.toString());
            l = aChar;
        }
        return z;
    }

}
