package com.company.Interview;

import java.util.stream.IntStream;

/**
 * Created by Михаил on 10.03.2017.
 */
public class TripletSum {
    public static void main(String[] args) {
        int[] a = {142, 712, 254, 869, 548, 645, 663, 758, 38, 860, 724, 742, 530, 779, 317, 36, 191, 843, 289, 107, 41, 943, 265, 649, 447, 806, 891, 730, 371, 351, 7, 102, 394, 549, 630, 624, 85, 955, 757, 841, 967, 377, 932, 309, 945, 440, 627, 324, 538, 539, 119, 83, 930, 542, 834, 116, 640, 659, 705, 931, 978, 307, 674, 387, 22, 746, 925, 73, 271, 830, 778, 574, 98, 513};
        int x = 165;
        System.out.println(tripletSum(x, a));
    }

    static boolean tripletSum(int x, int[] a) {
        boolean q = false;
        if (a.length > 3) {
            for (int i = 0; i < a.length; i++) {
                for (int j = 1; j < a.length; ) {
                    if (a[i] + a[j] + a[j + 1] != x) {
                        j++;
                    } else q = true;
                    break;
                }

            }
        } else {
            int total = IntStream.of(a).sum();
            if (total == x) {
                q=true;
            }
        }
        if (q == false) {
            return false;
        } else {
            return true;
        }
    }
}


