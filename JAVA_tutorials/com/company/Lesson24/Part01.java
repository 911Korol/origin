package com.company.Lesson24;

import javax.print.URIException;
import java.io.IOException;
import java.net.URISyntaxException;

/* Перехват checked исключений
В методе processExceptions обработайте все checked исключения.
Нужно вывести на экран каждое возникшее checked исключение.
Можно использовать только один блок try..
*/
public class Part01 {
    public static void main(String[] args) throws URISyntaxException {
        try {
            processExeptions();
        }catch (NullPointerException s){
            System.out.println("Pointer");
        }catch (URISyntaxException d){
            System.out.println("U_R_I");
        }catch (IndexOutOfBoundsException f){
            System.out.println("Index");
        }



    }
    public static void processExeptions() throws URISyntaxException {
        int a = (int) (Math.random()) * 3;
        if (a == 0){
            throw new NullPointerException();
        }
        if (a==1){
            throw new URISyntaxException("", "");
        }
        if (a == 2){
            throw new IndexOutOfBoundsException();
        }
    }
}
