package com.company.Lesson24;

/* Гласные и согласные буквы
Написать программу, которая вводит с клавиатуры строку текста.
Программа должна вывести на экран две строки:
1. первая строка содержит только гласные буквы
2. вторая - только согласные буквы и знаки препинания из введённой строки.
Буквы соединять пробелом, каждая строка должна заканчиваться пробелом.
public static char[] vowels = new char[]{'а', 'я', 'у', 'ю', 'и', 'ы', 'э', 'е', 'о', 'ё'};
Пример ввода:
Мама мыла раму.
Пример вывода:
а а ы а а у
М м м л р м .*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Part05_Char_Vowels {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        char[] chars = s.toCharArray();
        String vowel = "";
        String consonants = "";
        for (int i = 0; i < chars.length; i++) {
            if (isSame(chars[i])) {
                vowel += " " + chars[i] + " ";
            } else if (chars[i] == ' ') {
            } else consonants += " " + chars[i] + " ";
        }
        System.out.println(vowel);
        System.out.println(consonants);


    }

    static char[] vowels = new char[]{'а', 'я', 'у', 'ю', 'и', 'ы', 'э', 'е', 'о', 'ё'};

    public static boolean isSame(char a) {
        for (int i = 0; i < vowels.length; i++) {
            if (vowels[i] == a) {
                return true;
            }
        }
        return false;
    }

}
