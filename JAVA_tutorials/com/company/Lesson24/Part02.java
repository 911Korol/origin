package com.company.Lesson24;
/* Перехват unchecked исключений
В методе processExceptions обработайте все unchecked исключения.
Нужно вывести стек-трейс каждого возникшего исключения используя метод printStack.
Можно использовать только один блок try..
*/

public class Part02 {
    public static void main(String[] args) {
        try {
            process();
        } catch (UnknownError a) {
            System.out.println("unknown");
            stack(a);
        } catch (StackOverflowError s) {
            System.out.println("stack");
            stack(s);
        } catch (ThreadDeath d) {
            System.out.println("Thread");
            stack(d);
        }
    }

    public static void process() {
        int x = (int) (Math.random() * 3);
        if (x == 0) {
            throw new UnknownError();
        }
        if (x == 1) {
            throw new StackOverflowError();
        }
        if (x == 2) {
            throw new ThreadDeath();
        }

    }

    public static void stack(Throwable a) {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        for (StackTraceElement element : elements) {
            System.out.println(element);
        }
    }
}
