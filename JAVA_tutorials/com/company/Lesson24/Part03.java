package com.company.Lesson24;

/* Деление на ноль
Создай метод public static void divisionByZero,
в котором подели любое число на ноль и выведи на экран результат деления.
Оберни вызов метода divisionByZero в try..catch.
 Выведи стек-трейс исключения используя метод exception.printStackTrace()
*/
public class Part03 {
    public static void main(String[] args) {
        try{devide();
        }catch (ArithmeticException a){
            divisionByZero(a);
        }

    }
    public static void devide(){
        int a = 4/0;
        System.out.println(a);
    }
    public static  void divisionByZero(Throwable a){
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        for (StackTraceElement element : elements) {
            System.out.println(element);
        }
    }


}
