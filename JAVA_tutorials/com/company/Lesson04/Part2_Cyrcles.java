package com.company.Lesson04;

/*
 Рисуем прямоугольник
Ввести с клавиатуры два числа m и n.
Используя цикл for вывести на экран прямоугольник размером m на n из восьмёрок.
Пример: m=2, n=4
8888
8888


 */
public class Part2_Cyrcles {
    public static void main(String[] args) {


       for(int i = 0; i < 3; i++) {
           System.out.println("text " + i);
           for (int j = 0; j < 5; j++) {
               System.out.println("t " + j);
           }
       }
        System.out.println("----------------------");

        int a = 0;
        while(a < 10){
            System.out.println("text " + a);
            a++;
        }
        System.out.println("----------------------");

        int b = 0;
        do{
            System.out.println("text " + b);
            b++;
        }while(b < 10);
    }
}
