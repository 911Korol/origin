package com.company.Lesson04;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*/

Ввести с клавиатуры строку и число N.
Вывести на экран строку N раз используя цикл while.
Пример ввода:
абв
2
Пример вывода:
абв
абв


 */
public class Practic2 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String a = (reader.readLine());
        int b = (Integer.parseInt(reader.readLine()));
        int c = 0;
        while (c<b){
            System.out.println(a);
            c++;

        }

    }
}
