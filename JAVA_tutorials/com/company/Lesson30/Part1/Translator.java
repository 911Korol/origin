package com.company.Lesson30.Part1;

/**
 * Created by Михаил on 16.03.2017.
 */
public abstract class Translator {
    public abstract String getLanguage();
    public String translate(){
        String s = "Я переводчик с";
        return s + getLanguage();
    }
}
