package com.company.Lesson30.Part2;

/* Погода
1. Создать интерфейс Weather с методом getWeatherType().
2. Создать интерфейс WeatherType с перечнем погодных условий.
3. В классе Today реализовать интерфейс Weather.
4. В классе Today создать переменную String type;
5. Подумай, как связан параметр String type с методом getWeatherType().
6. Переопределить метод toString() в классе Today, что бы он выводил на экран надпись %search_for_pair_numbers for today, где %search_for_pair_numbers - тип погоды
*/
public class Part01 {
    public static void main(String[] args) {
        Weather today = new Today(WeatherType.rain);
        System.out.println(today.getWeathertype());
    }
}
