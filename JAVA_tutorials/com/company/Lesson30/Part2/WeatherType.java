package com.company.Lesson30.Part2;

/**
 * Created by Михаил on 16.03.2017.
 */
public interface WeatherType {
    String rain = "rain";
    String wind = "wind";
    String sun = "sun";
}
