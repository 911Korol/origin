package com.company.Lesson30.Part3;

import java.util.List;

/**
 * Created by Михаил on 16.03.2017.
 */
public class RepkaStory {
    public static void tell(List<Person> list){
        Person first = null;
        Person second = null;
        for (int i = list.size()-1; i > 0 ; i--) {
            first = list.get(i);
            second = list.get(i-1);
            first.pull(second);
        }
    }
}
