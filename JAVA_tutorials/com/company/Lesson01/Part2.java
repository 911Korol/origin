package com.company.Lesson01;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Part2 {
    public  static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите имя");

        String s1 = reader.readLine();

        System.out.println("Stroka 1 - " + s1);

        String s2 = reader.readLine(); // "56"
        int n = Integer.parseInt(s2);  //  56

        System.out.println(s2.length());
        System.out.println("Pow = " + n*n);

        int n2 = Integer.parseInt(reader.readLine());
        System.out.println(n - n2);
    }
}
