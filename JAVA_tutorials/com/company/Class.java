package com.company;
public class Class {


    public static strictfp void main(String[] args) throws RuntimeException {
        String a = "java";
        String b = "java";
        System.out.println(a.hashCode());
        System.out.println(b.hashCode());

        System.out.println(a == b);//true
        b += "a";
        System.out.println(a.equals(b));//false

        System.out.println(a == b);//false

        System.out.println(a.hashCode());
        System.out.println(b.hashCode());
    }
}