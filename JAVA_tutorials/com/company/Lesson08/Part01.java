package com.company.Lesson08;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by misha on 22.12.16.
 */
public class Part01 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] array = new int[5];
        for (int i = 0; i < array.length; i++) {
            array[i] = Integer.parseInt(reader.readLine());

        }

        int min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min){
                array[i] = min;
            }


        }
        System.out.println(min);
    }
}
