package com.company.Lesson17;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* Одинаковые имя и фамилия
Создать словарь (Map<String, String>) занести в него десять записей по принципу «Фамилия» - «Имя».
Проверить сколько людей имеют совпадающие с заданным имя или фамилию.
*/
public class Part01 {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        list1(map);
        findNames(map, "Simn");
        findLastName(map, "Simn");

    }
    public static Map list1(Map map){
        map.put("Simn", "Tomn");
        map.put("Simn", "Arbusn");
        map.put("Baby", "Simn");
        map.put("Art", "Simn");
        map.put("Sim", "Dogn");
        map.put("Eat", "Eatn");
        map.put("Food", "Foodn");
        map.put("Gevey", "Geveyn");
        map.put("Hugs", "Hugsn");
    return map;
    }

    public static void findNames (Map<String,String> map, String name){
        Iterator<Map.Entry<String,String>> itr = map.entrySet().iterator();
        int count = 0;
        while (itr.hasNext()){
            Map.Entry<String,String> entry = itr.next();
            if (name.equals(entry.getValue())){
                count++;
            }

        }
        System.out.println(count);
    }
    public static void findLastName(Map<String,String> map, String name){
        Iterator<Map.Entry<String,String>> itr = map.entrySet().iterator();
        int count = 0;
        while (itr.hasNext()){
            Map.Entry<String,String> entry = itr.next();
            if (name.equals(entry.getKey())){
                count++;
            }
        }
        System.out.println(count);
    }

}
