package com.company.Lesson17;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* Удалить всех людей, родившихся летом
Создать словарь (Map<String, Date>) и занести в него десять записей по принципу: «фамилия» - «дата рождения».
Удалить из словаря всех людей, родившихся летом.
*/
public class Part02 {
    public static void main(String[] args) {
        Map<String, Date> map = new HashMap<>();
        newDates(dates(map));
        for (Map.Entry<String, Date> entry : map.entrySet()) {
            System.out.println(entry);
        }
    }

    public static Map<String, Date> dates(Map<String, Date> map) {
        map.put("Stallone", new Date("JUNE 1 1980"));
        map.put("Stallone1", new Date("JUNE 5 1999"));
        map.put("Stallone2", new Date("JUNE 13 1996"));
        map.put("Stallone3", new Date("JUNE 1 1980"));
        map.put("Stallone4", new Date("JULY 1 1980"));
        map.put("Stallone5", new Date("AUGUST 1 1980"));
        map.put("Stallone6", new Date("JUNE 1 1980"));
        map.put("Stallone7", new Date("JUNE 1 1980"));
        map.put("Stallone8", new Date("DECEMBER 1 1980"));
        map.put("Stallone9", new Date("MARCH 1 1980"));
        return map;
    }

    public static Map<String, Date> newDates(Map<String, Date> map) {
        Iterator<Map.Entry<String, Date>> itr = map.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry<String, Date> entry = itr.next();
            Integer a = entry.getValue().getMonth();
            if (a >= 5 && a <= 7) {
                itr.remove();
            }

        }
        return map;
    }

}
