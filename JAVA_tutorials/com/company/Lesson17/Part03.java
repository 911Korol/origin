package com.company.Lesson17;

/* Удалить людей, имеющих одинаковые имена
Создать словарь (Map<String, String>) занести в него десять записей по принципу «фамилия» - «имя».
Удалить людей, имеющих одинаковые имена.
*/

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Part03 {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map = sort(names(map));
        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.println(entry);
        }
    }

    public static Map<String, String> names(Map<String, String> map) {
        map.put("Rooney", "Leo");
        map.put("Lloris", "Hugo");
        map.put("Messi", "Leo");
        map.put("Ronaldo", "Cristiano");
        map.put("Maldini", "Paolo");
        map.put("Indzaghi", "Pipo");
        map.put("Del Piero", "Alesandro");
        map.put("Balotelli", "Mario");
        map.put("Gotze", "Mario");
        map.put("Gomez", "Mario");
        return map;
    }

    public static Map<String, String> sort(Map<String, String> map) {
        Map<String, String> newMap = new HashMap<>();

        for (Map.Entry<String, String> entry : map.entrySet()) {
            Integer a = 0;
            for (String s : map.values()) {
                if (s.equals(entry.getValue())) {
                    a++;
                }
            }

            if (a == 1) {
                newMap.put(entry.getKey(), entry.getValue());
            }
        }
        return newMap;
    }
}
