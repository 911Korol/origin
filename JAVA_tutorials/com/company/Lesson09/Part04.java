package com.company.Lesson09;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* Один большой массив и два маленьких
1. Создать массив на 10 чисел.
2. Ввести в него значения с клавиатуры.
3. Создать два массива на 5 чисел каждый.
4. Скопировать большой массив в два маленьких: половину чисел в первый маленький, вторую половину во второй маленький.
5. Вывести второй маленький массив на экран, каждое значение выводить с новой строки.

 */
public class Part04 {
    public static void main(String[] args) throws IOException {
        int[] a = bigmass();
        int[] array1 = litmass(a);
        int[] array2 = litmass2(a);
        for (int i : array2) {
            System.out.println(i);
        }

    }

    public static int[] bigmass() throws IOException {
        int[] mass = new int[10];
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < mass.length; i++) {
            mass[i] = Integer.parseInt(reader.readLine());
        }
        return mass;
    }

    public static int[] litmass(int[] a) {
        int[] lmass = new int[5];
        for (int i = 0; i < lmass.length; i++) {
            lmass[i] = a[i];
        }
        return lmass;
    }

    public static int[] litmass2(int[] a) {
        int[] lmass2 = new int[5];
        for (int i = 0; i < lmass2.length; i++) {
            lmass2[i] = a[i + 5];
        }
        return lmass2;
    }
}