package com.company.Lesson09;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* Максимальное среди массива на 20 чисел
1. В методе initializeArray():
1.1. Создайте массив на 20 чисел
1.2. Считайте с консоли 20 чисел и заполните ими массив
2. Метод max(int[] array) должен находить максимальное число из элементов массива

 */
public class Part01 {
    public static void main(String[] args) throws IOException {
        int[] per = initializeArray();
        System.out.println(max(per));
    }

    private static int[] initializeArray() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] mass = new int[5];
        for (int i = 0; i < mass.length; i++) {
            mass[i] = Integer.parseInt(reader.readLine());

        }return mass;
    }

    public static int max (int[] array) {
        int comp = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] > comp) {
                comp = array[i];
            }


        } return comp;

    }
}
