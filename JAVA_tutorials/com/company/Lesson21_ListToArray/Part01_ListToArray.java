package com.company.Lesson21_ListToArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Михаил on 11.02.2017.
 */
public class Part01_ListToArray   {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<String> list = new ArrayList<>();
        while (true) {
            String s = reader.readLine();
            if (s.isEmpty()) {
                break;
            } else {
                list.add(s);
            }
        }
        String[] array = list.toArray(new String[(list.size())]);
        sort(array);
        for (String s : array) {
            System.out.println(s);
        }
    }

    public static void sort(String[] array) {
        for (int i = 0; i < array.length - 1; ) {
            int s = i + 1;
            if (isBigger(array[i], array[s])) {
                String b = array[s];
                array[s] = array[i];
                array[i] = b;
                if (i > 0) {
                    i--;
                }
            }else {
                i++;
            }
        }

    }


    public static boolean isBigger(String a, String b) {
        return a.compareTo(b) > 0;
    }
}