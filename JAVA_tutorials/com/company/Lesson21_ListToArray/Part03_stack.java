package com.company.Lesson21_ListToArray;

/**
 * Created by Михаил on 11.02.2017.
 * LIFO - last in first out ( stack )
 * FIFO - first in first out ( queue )
 * /* Каждый метод должен возвращать свой StackTrace
 Написать пять методов, которые вызывают друг друга. Каждый метод должен возвращать свой StackTrace.
 */

public class Part03_stack {
    public static void main(String[] args) {
        method1();
    }

    public static void method1(){
        method2();

        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        for (StackTraceElement element : elements) {
            System.out.println(element);
        }
    }
    public static void method2(){
        method3();
        StackTraceElement[] elements2 = Thread.currentThread().getStackTrace();
        for (StackTraceElement stackTraceElement : elements2) {
            System.out.println(stackTraceElement);
        }
    }
    public static void method3(){
        method4();
        StackTraceElement[] elements3 = Thread.currentThread().getStackTrace();
        for (StackTraceElement stackTraceElement : elements3) {
            System.out.println(stackTraceElement);
        }
    }

    public static void method4(){
        method5();
        StackTraceElement[] elements4 = Thread.currentThread().getStackTrace();
        for (StackTraceElement stackTraceElement : elements4) {
            System.out.println(stackTraceElement);
        }
    }

    public static void method5(){
        StackTraceElement[] elements5 = Thread.currentThread().getStackTrace();
        for (StackTraceElement stackTraceElement : elements5) {
            System.out.println(stackTraceElement);
        }
    }
}
