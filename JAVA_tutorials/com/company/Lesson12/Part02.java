package com.company.Lesson12;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* Удвой слова
1. Введи с клавиатуры 10 слов в список строк.
2. Метод doubleValues должен удваивать слова по принципу a,b,c -> a,a,b,b,c,c.
3. Используя цикл for выведи результат на экран, каждое значение с новой строки.
*/
public class Part02 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<String> list = new ArrayList<>();
        while (true){
            String s = reader.readLine();
            if(s.isEmpty()){
                break;
            }
            list.add(s);
        }
        list = doubleValues(list);

        for (String s : list) {
            System.out.println(s);
        }
    }
    public static List doubleValues(List<String> list2){
        List<String> list3 = new ArrayList<>();
        for (int i = 0; i < list2.size(); i++) {
            list3.add(list2.get(i));
            list3.add(list2.get(i));

        }return list3;
    }
}

