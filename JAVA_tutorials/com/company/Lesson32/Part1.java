package com.company.Lesson32;

import java.io.*;

/* Чтение файла
1. Считать с консоли имя файла.
2. Вывести в консоль(на экран) содержимое файла.
3. Если файла по заданному пути не существует, запросить ввод имени файла еще раз.
3. Не забыть освободить ресурсы. Закрыть поток чтения с файла и поток ввода с клавиатуры.
*/
public class Part1 {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        InputStream inputStream;
        try {
            while (true) {
                String s = reader.readLine();
                inputStream = new FileInputStream(s);
                break;
            }

            while (inputStream.available() > 0) {
                int z = inputStream.read();
                System.out.print((char) z);
            }
            inputStream.close();

        } catch (IOException a) {

        }
    }


}

