package com.company.Lesson05_Cycle;
/*
Рисуем треугольник
Используя цикл for вывести на экран прямоугольный треугольник из восьмёрок со сторонами 10 и 10.
Пример:
8
88
888
...

        */
public class Practic05 {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++){
            System.out.print("8");
        }
        System.out.println();
        for (int i = 0; i < 10; i++){
            System.out.println("8");
        }
            }
}
