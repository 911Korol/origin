package com.company.Lesson02;

/**
 * 4 method - + - * /
 */
public class Practic01 {
    public static void main(String[] args){
        int a = 5;
        int b = 7;
        System.out.println(plus(a,b));
        System.out.println(minus(b,a));
        System.out.println(umn(b,a,2));
        System.out.println(sep(7,2));
    }
    private static int plus (int a, int b){
        return a + b;
    }
    private static int minus (int a, int b){
        return a - b;
    }
    private static int umn (int f, int q, int c){
        return f * q * c;
    }
    private static double sep (double a, double b){
        return a / b;
    }

}
