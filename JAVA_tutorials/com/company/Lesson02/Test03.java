package com.company.Lesson02;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 0 method - kvadrat ( 5 -> 25)
 * 1 method - convert USD -> UAH (27.05)
 * 2 method - 10 procent (10 -> 11);
 * 3 method - 4 print string ( text - > sout 4 )
 */
public class Test03 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(kvadrat(Integer.parseInt(reader.readLine())));
        System.out.println(convert(Integer.parseInt(reader.readLine())));
        System.out.println(persent(Integer.parseInt(reader.readLine())));
        stroka(reader.readLine());


    }

    private static double kvadrat(double a) {
        return a * a;
    }

    private static double convert (double a){
        return a * 27.05;
    }

    private static double persent (double a){
        return a * 0.1 + a;
    }

    private static void stroka (String a){
        System.out.println(a);
        System.out.println(a);
        System.out.println(a);
        System.out.println(a);
    }
}