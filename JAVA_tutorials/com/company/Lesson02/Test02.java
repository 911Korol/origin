package com.company.Lesson02;

/**
 *
 */
public class Test02 {
    public static void main(String[] args) {
        swap(5, 7);
    }

    private static void swap(int a, int b){
        System.out.println(a + " " + b);
        int temp = a;
        a = b;
        b = temp;
        System.out.println(a + " " + b );
    }
}
