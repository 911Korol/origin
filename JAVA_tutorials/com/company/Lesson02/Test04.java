package com.company.Lesson02;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Реализуй метод public static void salary(int a).
 Метод должен увеличить переданное число на 100 и вывести на экран надпись:
 «Твоя зарплата составляет: a долларов в месяц.».
 Где a - это число, которое увеличили на 100.
 */
public class Test04 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        salary(Integer.parseInt(reader.readLine()));
    }
    public static void salary(int a){
        System.out.printf("«Твоя зарплата составляет %d долларов в месяц.».", a + 100);
    }

}
