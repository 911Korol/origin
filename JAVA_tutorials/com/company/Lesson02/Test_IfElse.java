package com.company.Lesson02;

/**
 * Created by misha on 26.11.16.
 */
public class Test_IfElse {
    public static void main(String[] args) {
        int a = 46;
        int b = 36;

        // < > == !=
        // && - AND true true
        // || - OR true false

        if(a > b){
            System.out.println("Max is " + a);
        } else if(a < b){
            System.out.println("Max is " + b);
        } else {
            System.out.println("Equals");
        }

        // ------ and -------- //
        if(a > 50 && a < 100){
            System.out.println("Ok");
        }else {
            System.out.println("Ne OK");
        }

        // -------- OR --------- //
        if(b < 50 || b > 100){
            System.out.println("Ok");
        } else {
            System.out.println("Ne Ok!");
        }

    }
}
