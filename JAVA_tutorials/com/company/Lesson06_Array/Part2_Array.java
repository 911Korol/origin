package com.company.Lesson06_Array;

/**
 * Created by misha on 15.12.16.
 */
public class Part2_Array {
    public static void main(String[] args) {
        int[] array = new int[5];
        array[2] = 76;
        array[0] = 12;
        System.out.println(array[2]);
        System.out.println(array[3]);

        int n = array.length;
        System.out.println(n);

        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }

    }
}
