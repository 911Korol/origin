package com.company.Lesson06_Array;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by misha on 15.12.16.
 */
public class Practic6 {
   static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    public static void main(String[] args) throws IOException {

        minimal();

    }

    public static void minimal() throws IOException {

        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());

        if (a < b) {
            System.out.println(a);
        } else System.out.println(b);
    }
}
