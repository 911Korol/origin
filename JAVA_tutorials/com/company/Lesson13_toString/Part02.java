package com.company.Lesson13_toString;

import java.util.ArrayList;
import java.util.List;

/* Семья
Создай класс Human с полями имя(String), пол(boolean),возраст(int), отец(Human), мать(Human).
Создай объекты и заполни их так, чтобы получилось: Два дедушки, две бабушки, отец, мать, трое детей. Вывести объекты на экран.
Примечание:
Если написать свой метод String toString() в классе Human, то именно он будет использоваться при выводе объекта на экран.
Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
…
*/
public class Part02 {
    public static void main(String[] args) {
        List<Human> list = new ArrayList<>();
        Human grandpa1 = new Human("Dan", true, 70, null, null);
        Human grandpa2 = new Human("Kel", true, 67, null, null);
        Human grandma1 = new Human("Dina", false, 65, null, null);
        Human grandma2 = new Human("Dana", false, 65, null, null);
        Human father = new Human("Igor", true, 40, grandpa1, grandma1);
        Human mother = new Human("Olga", false, 40, grandpa2, grandma2);
        Human child1 = new Human("Danil", true, 15, father, mother);
        Human child2 = new Human("Ruslan", true, 13, father, mother);
        Human child3 = new Human("Valdemar", true, 10, father, mother);
        list.add(grandpa1);
        list.add(grandpa2);
        list.add(grandma1);
        list.add(grandma2);
        list.add(father);
        list.add(mother);
        list.add(child1);
        list.add(child2);
        list.add(child3);

        for (Human human : list) {
            System.out.println(human);

        }

    }
    public static class Human{
        String name;
        Boolean sex;
        int age;
        Human father;
        Human mother;


        public Human(String name, Boolean sex, int age, Human father, Human mother){
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;

        }

        @Override
        public String toString() {
            String text = "";
            text += "Имя: " + name;
            if (sex){
                text += ", Пол: man";
            }else {
                text += ", Пол: woman";
            }
            text += ", Возраст: " + age;
            if (father != null ) {
                text += ", Папа: " + father.name;
            }
            if (mother != null ) {
                text += ", Мама: " + mother.name;
            }

            return text;
        }
    }
}
