package com.company.Lesson13_toString;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* Задача по алгоритмам
Задача: Написать программу, которая вводит с клавиатуры 10 чисел и выводит их в убывающем порядке.
*/
public class Part03 {
    public static void main(String[] args) throws IOException {
        int[] array = new int[5];
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < array.length; i++) {
            array[i] = Integer.parseInt(reader.readLine());
        }

        sort(array);

        for (int i : array) {
            System.out.println(i);
        }
    }

    private static void sort(int[] arr){                   // 5 3 7 1 -> 3 5 7 1 -> 3 5 1 7
        for (int i = 0; i < arr.length - 1; i++) {         // i = 0;                        i = 1;
            for (int j = 0; j < arr.length - 1 - i; j++) { // j = 0;     j = 1;    j = 2;
                if(arr[j] > arr[j + 1]){                   // 5 > 3 t; 5 > 7 f;  7 > 1 t;
                    int temp = arr[j];                     // temp = 5;  ---  ; temp = 7;
                    arr[j] = arr[j + 1];                   // [0] = 3;   ---  ;  [2] = 1;
                    arr[j + 1] = temp;                     // [1] = 5;   ---  ;  [3] = 7;
                }
            }
        }
    }
}
