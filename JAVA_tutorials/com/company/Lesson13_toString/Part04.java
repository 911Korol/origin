package com.company.Lesson13_toString;

/**
 * Created by misha on 14.01.17.
 *
 */
public class Part04 {
    public static void main(String[] args) {
        int a = 5; //7
        int b = 7; //1
        int c = 1; //5

        System.out.println(a + " " + b + " " + c);
        int d = a;
        a = b;
        b = c;
        c = d;
        System.out.println(a + " " + b + " " + c);
    }
}
