package com.company.Lesson40;

/**
 * Created by 007 on 17.04.2017.
 */

public final class Part01 implements Cloneable{
    final int a = 10;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Part01 part01 = (Part01) o;

        return a == part01.a;
    }

    @Override
    public int hashCode() {
        return a;
    }
}
