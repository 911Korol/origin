package com.company.Lesson31_FactoryMethodPattern.Part1;

/**
 * Created by Михаил on 18.03.2017.
 */
public interface Attackable {
    BodyPart attack();
}
