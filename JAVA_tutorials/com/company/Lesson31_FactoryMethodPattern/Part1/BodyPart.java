package com.company.Lesson31_FactoryMethodPattern.Part1;

/**
 * Created by Михаил on 18.03.2017.
 */
public class BodyPart {
    String bodyPart;

    public BodyPart(String bodyPart){

        this.bodyPart = bodyPart;
    }

    @Override
    public String toString() {
        return bodyPart;
    }

    final static BodyPart HAND = new BodyPart("hand");
    final static BodyPart LEG = new BodyPart("leg");
    final static BodyPart HEAD = new BodyPart("head");
    final static BodyPart CHEST = new BodyPart("chest");


}
