package com.company.Lesson31_FactoryMethodPattern.Part1;

/* Битва роботов
1. Создать класс Robot, BodyPart интерфейсы Attackable,Defensable и абстрактный класс AbstractRobot_FactoryMethod
2. В классе BodyPart создать переменную String bodyPart; добавить 4 объекта - части тела ( рука, нога, голова,
 грудь).
 Сделать объекты неизменяемыми.
3. В интерфейсе Attackable создать метод BodyPart attack();
4. В интерфейсе Defensable создать метод BodyPart defense();
5. В классе AbstractRobot_FactoryMethod создать переменные private String name; private static int hitCount;
6 В классе AbstractRobot_FactoryMethod создать логику атаки и защиты. Реализовать интерфейсы Attackable и Defensable
6.1 В методах attack() и defense() инициализировать переменную hitCount рандомным числом от 1 до 4.
    В зависимости от результата инициализации должна быть атакована и защищена определенная часть тела робота.
7. Унаследовать класс Robot от AbstractRobot_FactoryMethod. В классе Robot создать конструктор супер класса.
8. В выполняющем классе создать метод doMove(AbstractRobot_FactoryMethod robotFirst, AbstractRobot_FactoryMethod robotSecond).
8.1 В методе doMove реализовать логику вывода на экран надписи "%search_for_pair_numbers атаковал робота %search_for_pair_numbers, атакована %search_for_pair_numbers, защищена %search_for_pair_numbers"
*/
public class Part01 {
    public static void main(String[] args) {
        AbstractRobot_FactoryMethod robotFirst = new Robot("Rex", 100);
        AbstractRobot_FactoryMethod robonSecond = new Robot("Shprot", 200);
        doMove(robotFirst, robonSecond);
        doMove(robotFirst, robonSecond);
        doMove(robotFirst, robonSecond);
        doMove(robonSecond, robotFirst);
        doMove(robonSecond, robotFirst);
        doMove(robonSecond, robotFirst);

    }

    public static void doMove(AbstractRobot_FactoryMethod robotFirst, AbstractRobot_FactoryMethod robotSecond) {
        BodyPart attacked = robotFirst.attack();
        BodyPart defenced = robotSecond.defense();
        System.out.println(String.format("%search_for_pair_numbers атаковал робота %search_for_pair_numbers, атакована %search_for_pair_numbers, защищена %search_for_pair_numbers", robotFirst.getName(), robotSecond.getName(),
                attacked, defenced));

        if (attacked == defenced) {
            System.out.println(String.format("Робот %search_for_pair_numbers отразил удар", robotSecond.getName()));
        } else {
            System.out.println(String.format("Робот %search_for_pair_numbers пропустил удар", robotSecond.getName()));
        }
    }

}
