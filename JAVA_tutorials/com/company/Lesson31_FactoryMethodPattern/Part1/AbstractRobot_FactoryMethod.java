package com.company.Lesson31_FactoryMethodPattern.Part1;

import static com.company.Lesson31_FactoryMethodPattern.Part1.BodyPart.*;

/**
 * Created by Михаил on 18.03.2017.
 */
public abstract class AbstractRobot_FactoryMethod implements Attackable, Defensable {
    private String name;
    private int health;

    public String getName() {
        return name;
    }



    public AbstractRobot_FactoryMethod(String name, int health) {
        this.name = name;
        this.health=health;
    }
    private static int hitCount;
    @Override
    public BodyPart attack() {
        BodyPart attacked = null;
        hitCount = (int) (Math.random() * 4);
        if (hitCount == 1) {
            attacked = HAND;
        } else if (hitCount == 2) {
            attacked = LEG;
        } else if (hitCount == 3) {
            attacked = HEAD;
        } else if (hitCount == 0) {
            attacked = CHEST;
        }

        return attacked;
    }

    @Override
    public BodyPart defense() {
        BodyPart defenced = null;
        hitCount = (int) (Math.random() * 4);
        if (hitCount == 1) {
            defenced = HAND;
        } else if (hitCount == 2) {
            defenced = LEG;
        } else if (hitCount == 3) {
            defenced = HEAD;
        } else if (hitCount == 0) {
            defenced = CHEST;
        }

        return defenced;
    }


}
