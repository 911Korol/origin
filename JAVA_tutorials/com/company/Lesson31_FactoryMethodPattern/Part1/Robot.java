package com.company.Lesson31_FactoryMethodPattern.Part1;

/**
 * Created by Михаил on 18.03.2017.
 */
public class Robot extends AbstractRobot_FactoryMethod {
    public Robot(String name, int health) {
        super(name, health);
    }
}
