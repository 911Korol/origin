package com.company.Lesson10_array_vs_list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Михаил on 05.01.2017.
 * 2(3) -> 3(5) -> 5(8)
 */
public class Part02_array_vs_list {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();  // int[] list = new int[-];
        System.out.println(list.size());
        list.add(56); // list[1] = 56; - // Dobavlenie v konec
        list.add(0, 24); // list[0] = 24 Vstavka
        list.add(2, 94); // list[2] = 94 -
        System.out.println(list.size());

        list.set(2, 50); // list[2] = 50
        list.remove(1); // -

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i)); // array[i]
        }
    }
}
