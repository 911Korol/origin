package com.company.Lesson10_array_vs_list;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**Ввести 5 строк с клавиатуры и вывести их на экран в обратном порядке
 */
public class Part04 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(reader.readLine());

        }
        for (int i = list.size() - 1; i >= 0; i--) {
            System.out.println(list.get(i));

        }
    }
}
