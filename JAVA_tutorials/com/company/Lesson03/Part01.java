package com.company.Lesson03;

/**
 * Created by misha on 01.12.16.
 * 2 cat - name, age, weight
 */
public class Part01 {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Barsik", 7, 9);
        Cat cat2 = new Cat("Murzik", 4, 7);

        System.out.println(cat1.name + " " + cat1.age + " " + cat1.weight);
        System.out.println(cat2.name + " " + cat2.age + " " + cat2.weight);
    }



   public static class Cat {
       String name;
       int age;
       int weight;

       public Cat(String name, int age, int weight) {
           this.name = name;
           this.age = age;
           this.weight = weight;
       }
   }
}
