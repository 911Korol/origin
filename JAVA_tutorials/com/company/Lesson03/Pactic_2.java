package com.company.Lesson03;

/**
 * Created by misha on 01.12.16.
 * 4 cl:
 * Woman - name, age
 * Cat - name, age, owner
 * Dog - name, age, owner
 * Fish - name, age, owner
 */
public class Pactic_2 {
    public static void main(String[] args) {
        Woman woman = new Woman("Eva", 35);
        Cat cat = new Cat("Tisha", 7, woman);
        Dog dog = new Dog("Rex", 7, woman);
        Fish fish = new Fish("Ariel", 8, woman);
        System.out.println(woman.name + woman.age);
        System.out.println(cat.name + cat.age + cat.owner.name);
        System.out.println(dog.name + dog.age + dog.owner.name);
        System.out.println(fish.name + fish.age + fish.owner.name);

    }

    public static class Woman {
        String name;
        int age;

        public Woman(String name, int age) {
            this.name = name;
            this.age = age;
        }
    }

    public static class Cat {
        String name;
        int age;
        Woman owner;

        public Cat(String name, int age, Woman owner) {
            this.name = name;
            this.age = age;
            this.owner = owner;

        }
    }

    public static class Dog {
        String name;
        int age;
        Woman owner;

        public Dog(String name, int age, Woman owner) {
            this.name = name;
            this.age = age;
            this.owner = owner;

        }
    }

    public static class Fish {
        String name;
        int age;
        Woman owner;

        public Fish(String name, int age, Woman owner) {
            this.name = name;
            this.age = age;
            this.owner = owner;
        }
    }
}
