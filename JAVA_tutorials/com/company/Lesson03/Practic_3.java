package com.company.Lesson03;

/**
 * Created by misha on 01.12.16.
 * Man - name, age, wife
 * Woman = name, age, husband
 */
public class Practic_3 {
    public static void main(String[] args) {
        Man man = new Man("Alesha",30, null);
        Woman woman = new Woman ("Alla",40,man);
        man.wife = woman;
        System.out.println(man.name + " " + man.age + " " + man.wife.name);
        System.out.println(woman.name + " " + woman.age + " " + woman.husband.name);


    }
    public static class Man{
        String name;
        int age;
        Woman wife;

        public Man(String name, int age, Woman wife){
            this.name = name;
            this.age = age;
            this.wife = wife;
        }

    }

    public static class Woman{
        String name;
        int age;
        Man husband;

        public Woman (String name, int age, Man husband){
            this.name = name;
            this.age = age;
            this.husband = husband;
        }


    }
}
