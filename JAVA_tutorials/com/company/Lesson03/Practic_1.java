package com.company.Lesson03;

/**
 * Created by misha on 01.12.16.
 */
public class Practic_1 {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Murzik", 8, 7);
        Cat cat2 = new Cat("Barsik", 7,6);
        System.out.println(cat1.name + " is " + cat1.age + " years old " + cat1.weight);
        System.out.println(cat2.name + " " + cat2.age + " " + cat2.weight);

    }
    private static class Cat {
        String name;
        int age;
        int weight;

        public Cat(String name, int age, int weight) {
            this.name = name;
            this.age = age;
            this.weight = weight;
        }
    }
}
