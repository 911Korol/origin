package com.company.Lesson27.Task01;

/**
 * Created by Михаил on 04.03.2017.
 */
public  class Horse {
    String name;
    boolean flyable;
    boolean runnable;


    public Horse(String name, boolean flyable, boolean runnable) {
        this.name = name;
        this.flyable = flyable;
        this.runnable = runnable;
    }

    public void cheking() {
        if (flyable) {
            System.out.println("My name is + this.name + , i can fly =)");
        } else {
            System.out.println("My name is + this.name + , i can't fly =)");
        }
    }
}
