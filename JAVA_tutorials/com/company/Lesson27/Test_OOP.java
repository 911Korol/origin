package com.company.Lesson27;

/**
 * Created by Михаил on 04.03.2017.
 */
public class Test_OOP {
    public static void main(String[] args) {
        Pet pet = new Pet();
        pet.method1();
    }

    public static class Pet{
        private int x = 5;

        private Pet() {
        }

        public void method1(){
            System.out.println("Animal");
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }
    }

    public static class Cat extends Pet{
        public void method1() {
            setX(10);
            System.out.println("Cat");
        }
    }
}
