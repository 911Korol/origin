package com.company.Lesson27.Task03;

/* Создать классы Cat и Dog с параметрами name и speed
Скрыть все внутренние переменные класса Cat и Dog.
 Также скрыть все методы, кроме тех, с помощью которых эти классы взаимодействуют друг с другом.
 Создать метод isDogNear в классе Cat, который возвращает true, если скорость кота больше
 Создать метод isCatNear в классе Dog, который возвращает true, если скорость собаки больше
*/
public class Part01 {
    public static void main(String[] args) {
        Dog dog = new Dog("Rex", 50);
        Cat cat = new Cat("Tisha", 30);
        if(dog.isCatNear(cat)){
            System.out.println("Winner is " + cat.getName());
        }else if (cat.isDogNear(dog)){
            System.out.println("Winner is " + dog.getName());
        }


    }
}
