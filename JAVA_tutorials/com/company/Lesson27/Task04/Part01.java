package com.company.Lesson27.Task04;

/*
Создать 2 класса Сow и Whale.
В классе Cow создать метод getName(), который возвращает строку "Я - корова".
Унаследовать Whale от Cow
Переопределить метод getName в классе Whale(Кит), чтобы программа выдавала:
Я не корова, Я - кит.
*/
public class Part01 {
    public static void main(String[] args) {
        Cow cow = new Cow();
        Cow whale = new Whale();

        System.out.println(cow.getName());
        System.out.println(cow.getName1());
        System.out.println(whale.getName());
        System.out.println(whale.getName1());
    }
}
