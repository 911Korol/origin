package com.company.Lesson27.Task02;

/* Ничего не поменяешь
1. Создать класс Cat с переменными name, age, weight.
2. Скрыть все внутренние переменные класса Cat:
- создать геттеры и сеттеры
- скрыть методы, позволяющие менять внутреннее состояние объектов класса Cat.
*/
public class Cat {
    private String name;
    private int age;
    private int weight;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Cat(String name, int age, int weight){
        this.name = name;
        this.age = age;
        this.weight = weight;

    }
}
