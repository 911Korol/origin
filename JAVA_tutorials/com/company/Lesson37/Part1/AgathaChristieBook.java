package com.company.Lesson37.Part1;



/**
 * Created by 007 on 08.04.2017.
 */
public class AgathaChristieBook extends Book {
    private String bookname2;
    public AgathaChristieBook(String bookname2){
        super("Agatha Christie");
        this.bookname2 = bookname2;
    }

    @Override
    public Book getBook() {
        return this;
    }

    @Override
    public String getName() {
        return bookname2;
    }


}
