package com.company.Lesson37.Part1;

/**
 * Created by 007 on 08.04.2017.
 */
public class MarkTwainBook extends Book {
    private String bookname;
    public MarkTwainBook(String bookname) {
        super("Mark Twain");
        this.bookname = bookname;
    }
    @Override
    Book getBook() {
        return this;
    }

    @Override
    String getName() {
        return bookname;
    }
}
