package com.company.Lesson37.Part1;


/**
 * Created by 007 on 08.04.2017.
 */
public abstract class Book {
    private  String author;
    public Book(String author){
        this.author = author;
    }
    abstract Book getBook();
    abstract String getName();
    String getOutputByBookType(){
        String agathaChristieOutput = author + ", " + getBook().getName() + " is a detective";
        String markTwainOutput = getBook().getName() + " book was written by " + author;
        String output;
        if (this instanceof MarkTwainBook){
          output = markTwainOutput;
        }else {
            output = agathaChristieOutput;
        }
        return output;
    }
}
