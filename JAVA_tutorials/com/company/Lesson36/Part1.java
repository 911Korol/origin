package com.company.Lesson36;

/**
 * Created by 007 on 06.04.2017.
 */

public class Part1 {

}
class Parent{
     int x = 1;
    public Parent(){
        x += 2;
    }
}
class Child extends Parent{
    public Child(){
        x += 1;
    }

    public static void main(String[] args) {
        Child c = new Child();
        System.out.println(c.x);
    }
}