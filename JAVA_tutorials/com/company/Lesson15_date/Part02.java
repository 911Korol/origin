package com.company.Lesson15_date;

/*/* Вывести на экран список значений
Есть коллекция HashMap<String, String>, туда занесли 10 различных строк.
Вывести на экран список значений, каждый элемент с новой строки.
*/

import java.util.HashMap;
import java.util.Map;

public class Part02 {
    public static void main(String[] args) {
        Map<String,String> map = new HashMap<>();
        map.put("kmkdyc", "fcefc");
        map.put("kmktdc", "fcefc");
        map.put("kmrkdc", "fcefc");
        map.put("kemkdc", "fcefc");
        map.put("wkmkdc", "fcefc");
        map.put("kdc", "fcefc");
        map.put("kkdc", "fcefc");
        map.put("kmkudc", "fcefc");
        for (String s : map.values()) {
            System.out.println(s);
        }

    }
}
