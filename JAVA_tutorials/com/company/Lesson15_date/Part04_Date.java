package com.company.Lesson15_date;

import java.util.Date;

/**
 * Created by misha on 21.01.17.
 */
public class Part04_Date {
    public static void main(String[] args) throws InterruptedException {
        Date date = new Date();
//      System.out.println("Today: " + date);
        Thread.sleep(3000);
        Date date1 = new Date();
        long msDistance = date1.getTime() - date.getTime();
        System.out.println(msDistance);
    }
}
