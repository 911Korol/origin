package com.company.Lesson15_date;

import java.util.Date;

/**
 * Created by misha on 21.01.17.
 */
public class Part08 {
    public static void main(String[] args) {
        Date date = new Date();
        Date date1 = new Date();
        date.setDate(1);
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        int msDay = 24 * 60 * 60 * 1000;
        long dateMs = date1.getTime() - date.getTime();
        System.out.println(dateMs/msDay);
    }
}
