package com.company.Lesson15_date;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by misha on 21.01.17.
 */
public class Part03 {
    public static void main(String[] args) {
        Map<String,String> map = new HashMap<>();
        map.put("qwer", "qwer");
        map.put("qwert", "qwert");
        map.put("qwerty", "qwerty");
        Iterator<Map.Entry<String,String>> iter = map.entrySet().iterator();
        while (iter.hasNext()){
            Map.Entry<String, String> entry = iter.next();
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }
}
