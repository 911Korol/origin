package com.company.Lesson15_date;

import java.util.Date;

/*Сколько прошло времени с начала сегодняшнего дня
 */
public class Part07 {
    public static void main(String[] args) {
        Date day = new Date();
        System.out.println("Today: " + day.getHours() + ":" + day.getMinutes() + ":" + day.getSeconds());
    }
}
