package com.company.Lesson15_date;

import java.util.Date;

/**
 * Created by misha on 21.01.17.
 */
public class Part06 {
    public static void main(String[] args) throws InterruptedException {
        Date startDate = new Date();

        long endTime = startDate.getTime() + 6000;
        Date endDate = new Date(endTime);

        Thread.sleep(7000);

        Date currentDate = new Date();
        if(currentDate.after(endDate)){
            System.out.println("Lose!");
        } else {
            System.out.println("Win!");
        }
    }
}
