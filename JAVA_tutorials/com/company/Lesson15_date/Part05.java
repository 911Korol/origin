package com.company.Lesson15_date;

import java.util.Date;

/**
 * Created by misha on 21.01.17.
 */
public class Part05 {
    public static void main(String[] args) throws InterruptedException {
        Date date = new Date();
        Thread.sleep(1000);
        Date date1 = new Date();
        System.out.println(date1.getTime() - date.getTime());
    }
}
