package com.company.Lesson15_date;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* Провести 10 тысяч вставок, удалений
Для arrayList и linkedList провести 10 тысяч вставок, удалений, а также вызовов get и set.
*/
/* Измерить сколько времени занимает 10 тысяч вставок для каждого списка
Измерить, сколько времени занимает 10 тысяч вставок для каждого списка.
Метод getTimeMsOfInsert  должен вернуть время его исполнения в миллисекундах.
*/
public class Part09 {
    public static void main(String[] args) {
        List list = new ArrayList();
        insert10_000(list);
        get_1(list);
        set1(list);
        rem(list);
        List list1 = new LinkedList();
        insert10_000(list1);
        get_1(list1);
        set1(list1);
        rem(list1);
    }

    public static void insert10_000(List list){
        for (int i = 0; i < 10_000; i++) {
            list.add(0, new Object());
        }
    }

    public static void get_1(List list){
        for (int i = 0; i < list.size(); i++) {
            list.get(i);
        }
    }

    public static void set1(List list){
        for (int i = 0; i < 10_000; i++) {
            list.set(i, new Object());
        }

    }

    public static void rem(List list){
        for (int i = 0; i < 10_000; i++) {
            list.remove(0);
        }
    }


}
