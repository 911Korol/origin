package com.company.Lesson15_date;

import java.util.HashMap;
import java.util.Map;

/* Вывести на экран список ключей
Есть коллекция HashMap<String, String>, туда занесли 3 различные строки.
При помощи метода printKeys:
Вывести на экран список ключей, каждый элемент с новой строки.
*/
public class Part01 {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("Ukraine", "Kyiv");
        map.put("Russia", "Moscow");
        map.put("USA", "Wasington");
        printKeys(map);
    }

    public static void printKeys(Map<String, String> map){
        for (String s : map.keySet()) {
            System.out.println(s);
        }
    }
}

