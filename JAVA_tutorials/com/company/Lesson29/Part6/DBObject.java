package com.company.Lesson29.Part6;

/**
 * Created by Михаил on 11.03.2017.
 */
public interface DBObject {
    DBObject initializeIdAndName(long id, String name);
}
