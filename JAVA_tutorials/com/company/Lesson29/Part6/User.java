package com.company.Lesson29.Part6;

/**
 * Created by Михаил on 11.03.2017.
 */
public class User implements DBObject {
    long id;
    String name;

    @Override
    public String toString() {
        return String.format("User has name %search_for_pair_numbers, id = %d", name, id);
    }

    @Override
    public User initializeIdAndName(long id, String name) {
        this.id = id;
        this.name = name;
        return this;
    }


}
