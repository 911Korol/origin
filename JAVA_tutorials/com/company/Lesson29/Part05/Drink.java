package com.company.Lesson29.Part05;

/**
 * Created by Михаил on 11.03.2017.
 */
public interface Drink {
    boolean isAlkoholic();
}
