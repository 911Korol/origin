package com.company.Lesson29.Part05;

/* Пиво и кола
Создать интерфейс Drink и классы Cola и Beer
В интерфейсе Drink создай метод isAlcoholic();
Реализуй интерфейс Drink в классах Beer и Cola.
Создать метод print, который должен определить какой напиток пришел ему в параметрах, алкогольный или безалкогольный.
И вывести на экран соответствующую запись.
*/
public class Drinks {
    public static void main(String[] args) {
        System.out.println(prinT(new Cola()));
        System.out.println(prinT(new Beer()));

    }
    public static String prinT(Drink x){
        String z = x.getClass().getSimpleName();
        if (x.isAlkoholic()){
            z+=" Alkoholic";
        }else {
            z+=" Nonalkoholic";
        }
        return z;
    }
}

