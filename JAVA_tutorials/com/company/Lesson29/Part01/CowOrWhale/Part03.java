package com.company.Lesson29.Part01.CowOrWhale;

/* Или «Корова», или «Кит», или «Собака», или «Неизвестное животное»
Написать метод, который определяет, объект какого класса ему передали, и возвращает результат – одно значение из: «Корова», «Кит», «Собака», «Неизвестное животное».
*/

import java.util.Objects;

public class Part03 {
    public static void main(String[] args) {
what(new Cow());
what(new Dog());
what(new Unknown());
    }

    public static void what(Object x){
        if (x instanceof Cow){
            System.out.println("Cow");
        }if (x instanceof Unknown){
            System.out.println("Unknown");
        }if (x instanceof Dog){
            System.out.println("Dog");
        }
    }
    public static class Cow{}
    public static class Dog{}
    public static class Unknown{}

}
