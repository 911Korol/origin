package com.company.Lesson29.Part01.CowOrWhale;

/* Или «Кошка», или «Собака», или «Птица», или «Лампа»
Написать метод, который определяет, объект какого класса ему передали,
и выводит на экран одну из надписей: Кошка, Собака, Птица, Лампа.
*/
public class Part01 {
    public static void main(String[] args) {
        method1(new Cat());

    }

    public static void method1(Object a){
        if(a instanceof Pet){
            System.out.println("Pet");
        } if(a instanceof Cat){
            System.out.println("Cat");
        } if(a instanceof Dog){
            System.out.println("Dog");
        }
    }

    public static class Pet{}
    public static class Cat extends Pet{}
    public static class Dog extends Pet{}
}

//class A{
//    public A(int i) {
//        i = 10;
//        System.out.println("A " + i);
//    }
//}
//
//class B extends A{
//    public B() {
//        super(5);
//        System.out.println("B");
//    }
//}
