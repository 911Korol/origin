package com.company.Lesson29.Part01.CowOrWhale;
/* Или «Кошка», или «Собака», или «Птица», или «Лампа»
Написать метод, который определяет, объект какого класса ему передали,
и выводит на экран одну из надписей: Кошка, Собака, Птица, Лампа.
*/

import java.util.Objects;

public class Part02 {
    public static void main(String[] args) {
        what(new Cat());
        what(new Dog());
        what(new Bird());
        what(new Lamp());
    }
    public static void what(Object x){
        if (x instanceof Cat){
            System.out.println("Cat");
        }if (x instanceof Dog){
            System.out.println("Dog");
        }if (x instanceof Bird){
            System.out.println("Bird");
        }if (x instanceof Lamp){
            System.out.println("Lamp");
        }
    }
    public static class Cat{}
    public static class Dog{}
    public static class Bird{}
    public static class Lamp{}


}
