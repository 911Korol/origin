package com.company.Lesson29.Part4;

/**
 * Created by Михаил on 11.03.2017.
 */
public class AlkoholicBeer implements Drink {

    @Override
    public String toString() {
        if (isAlcoholic()){
            return "Alkohol";
        }else {
            return "nonalkohol";
        }
    }

    @Override
    public boolean isAlcoholic() {
        return true;
    }

}
