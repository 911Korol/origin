package com.company.Lesson29.Part4;

/*
    Создать интерфейс Drink и класс AlcoholicBeer
    В интерфейсе Drink создай метод isAlcoholic();
    Добавь к классу AlcoholicBeer интерфейс Drink и реализуй все нереализованные методы.
    Метод  toString в классе AlcoholicBeer должен выводить на экран "Напиток алкогольный",
    если isAlcoholic() возвращает true,
    иначе вывести на экран надпись "Напиток безалкогольный".
*/
public class Part04 {
    public static void main(String[] args) {
        AlkoholicBeer beer = new AlkoholicBeer();
        System.out.println(beer);
    }
}
