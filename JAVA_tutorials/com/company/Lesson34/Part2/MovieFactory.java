package com.company.Lesson34.Part2;

/**
 * Created by Михаил on 30.03.2017.
 */
public class MovieFactory {
    static Movie getMovie(String key){
        Movie x = null;
        if (key.equals("cartoon")){
            x = new Cartoon();
        }if (key.equals("thriller")){
            x= new Thriller();
        }if (key.equals("soapOpera")) {
            x= new SoapOpera();
        }
        return x;
    }
}
