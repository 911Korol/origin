package com.company.Lesson34.Part1.Part1;

/* Мосты
1. Создать интерфейс Bridge с методом int getCarsCount().
2. Создать классы WaterBridge и SuspensionBridge, которые реализуют интерфейс Bridge.
3. Метод getCarsCount() должен возвращать любое захардкоженое значение типа int
4. Метод getCarsCount() должен возвращать различные значения для различных классов
5. В классе Solution создать публичный метод println(Bridge bridge).
6. В методе println вывести на консоль значение getCarsCount() для объекта bridge.*/

public class Part01 {
    public static void main(String[] args) {
        Bridge bridge1 = new WaterBridge();
        Bridge bridge2 = new SuspensionBridge();
        System.out.println(println(bridge1));
        System.out.println(println(bridge2));


    }
    public static int println(Bridge bridge) {
        return bridge.getCarsCount();
    }
}
