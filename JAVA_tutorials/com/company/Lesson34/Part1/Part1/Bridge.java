package com.company.Lesson34.Part1.Part1;

/**
 * Created by Михаил on 25.03.2017.
 */
public interface Bridge {

    int getCarsCount();
}
