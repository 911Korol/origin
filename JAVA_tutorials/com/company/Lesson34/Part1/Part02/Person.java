package com.company.Lesson34.Part1.Part02;

/**
 * Created by Михаил on 25.03.2017.
 */
public interface Person {
    class User implements Person {
        static void live() {
            System.out.println("Usually I just live");
        }
    }

     class Looser implements Person {
         static void doNothing() {
            System.out.println("Usually I do nothing.");
        }
    }

    class Coder implements Person {
        static void coding() {
            System.out.println("Usually I create code.");
        }
    }

    class Proger implements Person {
        static void enjoy() {
            System.out.println("Wonderful life!.");
        }
    }
}
