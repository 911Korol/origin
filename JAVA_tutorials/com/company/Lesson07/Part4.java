package com.company.Lesson07;

/*
 */
public class Part4 {
    public static void main(String[] args) {
        int[] array = {167, 92, 33, 564, 75, 90};

        int min = array[0];
        for (int i = 1; i < array.length; i++) {
            if (min > array[i]) {
                min = array[i];
            }
        }
        System.out.println(min);

    }
}
